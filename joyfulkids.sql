-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 07, 2015 at 08:06 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `joyfulkids`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(150) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `dc` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`, `dc`) VALUES
(2, 'joyfulkids', '76350372a8df088856ed83ebe363e9ded095b6b3', 'JoyfulKids', '2015-09-28 23:53:45');

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_album` varchar(200) NOT NULL,
  `tahun` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`id`, `nama_album`, `tahun`) VALUES
(2, 'Test2', 2015),
(3, 'Test3', 2015),
(5, 'Test lagi satu', 2015),
(6, 'coba lagi', 2015);

-- --------------------------------------------------------

--
-- Table structure for table `album_photos`
--

CREATE TABLE IF NOT EXISTS `album_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `keterangan_foto` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `album_photos`
--

INSERT INTO `album_photos` (`id`, `album_id`, `keterangan_foto`, `file`) VALUES
(5, 2, 'sadasda', 'bg.jpg'),
(6, 2, 'Coba coba', '683456139ac992b98.jpg'),
(8, 3, 'saya', '268405613a98d7bc5c.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  `review` text NOT NULL,
  `content` text NOT NULL,
  `waktu_pembuatan` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `judul`, `path`, `review`, `content`, `waktu_pembuatan`) VALUES
(6, 'hahahaha', 'hahahaha', '', 'hahahahhaa', '2015-09-26 08:34:36'),
(7, 'hahahaha', 'hahahaha', '', 'hahahahhaa', '2015-09-26 08:34:36'),
(8, 'hahahaha', 'hahahaha', '', 'hahahahhaa', '2015-09-26 08:34:36'),
(9, 'hahahaha', 'hahahaha', '', 'hahahahhaa', '2015-09-26 08:34:36'),
(10, 'hahahaha', 'hahahaha', '', 'hahahahhaa', '2015-09-26 08:34:36'),
(14, 'Dota 2 Aja Lahh', 'dota2-aja-lah', 'hahaha', '<p>&nbsp;This is my textarea to be replaced with CKEditor.<a href="http://japanesestation.com/wp-content/uploads/2015/09/Para-Wanita-Jepang-Merasa-Malu-Jika-Tertangkap-Melakukan-10-Hal-Ini-Sendirian-featured.jpg" target="_blank"><img alt="" src="http://japanesestation.com/wp-content/uploads/2015/09/Para-Wanita-Jepang-Merasa-Malu-Jika-Tertangkap-Melakukan-10-Hal-Ini-Sendirian-featured.jpg" style="float:left; height:166px; width:300px" /></a>&nbsp;asdasf</p>\r\n\r\n<p>sdfsadfsdfsdf sdafdsf sagsa</p>\r\n\r\n<p>&nbsp;gasgsdag</p>\r\n\r\n<p>sadg</p>\r\n\r\n<p>sag</p>\r\n\r\n<p>sad</p>\r\n\r\n<p>gssadsfasdfsadfsadfjsadhfljkshd fsajdhfkjasdhfkjshak jfkjfhfhff fdhfjhdhfuhsdkjfhdhghjsagdydgws sdfkhjdgs fgweiuyg hsgdf gswdf gswd</p>\r\n', '2015-09-26 17:57:55');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `path` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `waktu_pembuatan` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `judul`, `path`, `content`, `waktu_pembuatan`) VALUES
(1, 'About Us', 'AboutUs', '<p>hmmm my name is Putu Widnyana Santika dor dor dor</p>\r\n', '2015-09-29 03:44:00'),
(5, 'Carrier', 'Carrier', '<p>Hayyy Carrieer</p>\r\n', '2015-09-28 15:13:29'),
(7, 'TK/Play Group', 'tk-playgroup', '<p>Hallo anak anak</p>\r\n', '2015-09-29 09:16:16'),
(8, 'English Course for Children', 'english-course-for-children', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sem ex, sollicitudin vel ultrices vel, volutpat in tellus. Aliquam feugiat turpis volutpat, ultrices ex ac, euismod turpis. Donec in elit vitae arcu fringilla blandit tempus et odio. Pellentesque congue fermentum arcu, et posuere mauris efficitur vitae. Nam ac iaculis libero. Phasellus commodo, ligula nec eleifend maximus, tellus velit commodo elit, sit amet ullamcorper mauris nulla et nibh. Pellentesque tempor, mauris vitae tempus imperdiet, metus massa imperdiet tortor, vitae finibus est massa et mauris.</p>\r\n', '2015-10-06 12:48:28'),
(9, 'Kursus Matematika', 'kursus-matematika', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sem ex, sollicitudin vel ultrices vel, volutpat in tellus. Aliquam feugiat turpis volutpat, ultrices ex ac, euismod turpis. Donec in elit vitae arcu fringilla blandit tempus et odio. Pellentesque congue fermentum arcu, et posuere mauris efficitur vitae. Nam ac iaculis libero. Phasellus commodo, ligula nec eleifend maximus, tellus velit commodo elit, sit amet ullamcorper mauris nulla et nibh. Pellentesque tempor, mauris vitae tempus imperdiet, metus massa imperdiet tortor, vitae finibus est massa et mauris.</p>\r\n', '2015-10-06 12:49:29'),
(10, 'Ca-Lis-Tung', 'ca-lis-tung', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sem ex, sollicitudin vel ultrices vel, volutpat in tellus. Aliquam feugiat turpis volutpat, ultrices ex ac, euismod turpis. Donec in elit vitae arcu fringilla blandit tempus et odio. Pellentesque congue fermentum arcu, et posuere mauris efficitur vitae. Nam ac iaculis libero. Phasellus commodo, ligula nec eleifend maximus, tellus velit commodo elit, sit amet ullamcorper mauris nulla et nibh. Pellentesque tempor, mauris vitae tempus imperdiet, metus massa imperdiet tortor, vitae finibus est massa et mauris.</p>\r\n', '2015-10-06 12:50:26'),
(11, 'Kursus Menggambar', 'kursus-menggambar', '<p>Anak-anak belajar mewarnai dan menggambar di tempat yang nyaman dan dekat dengan lingkungan, diajar oleh pakar menggambar.</p>\r\n', '2015-10-07 15:54:22');

-- --------------------------------------------------------

--
-- Table structure for table `home_menu_item`
--

CREATE TABLE IF NOT EXISTS `home_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `keterangan` text NOT NULL,
  `link` varchar(200) NOT NULL,
  `warna_body` varchar(8) NOT NULL,
  `warna_header` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `home_menu_item`
--

INSERT INTO `home_menu_item` (`id`, `judul`, `keterangan`, `link`, `warna_body`, `warna_header`) VALUES
(3, 'Kursus Menggambar', 'Anak-anak belajar mewarnai dan menggambar di tempat yang nyaman dan dekat dengan lingkungan, diajar oleh pakar menggambar.', 'content/info/kursus-menggambar', '9bd3a4', '6aba77'),
(5, 'English Course for Children', 'Lokasi yang aman untuk anak-anak, setiap kelas tersedia tape recorder, jumlah murid dalam 1 kelas 10 orang, buku yang menarik dan disesuaikan dengan usia anak.', '/content/info/english-course-for-children', 'c29e70', 'A25F08'),
(6, 'Kursus Matematika', 'Satu kelas 10 orang, diajar oleh guru lulusan FKIP yang berkualitas, materi mengikuti kurikulum nasional, bagi anak kelas 6 diberi latihan UAN.', 'content/info/kursus-matematika', '88b9e5', '5b93c4'),
(7, 'Kursus Ca-Lis-Tung', 'Anak-anak diajar membaca, menulis dan berhitung dengan metode yang mudah dimengerti oleh anak, untuk menguji kemampuan anak membaca maka anak diuji.', 'content/info/ca-lis-tung', 'f9e69a', 'f2d253');

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(128) NOT NULL,
  `alt` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `image`
--

INSERT INTO `image` (`id`, `file_path`, `alt`) VALUES
(1, '31490560a23b01985d.jpg', 'asdasfra'),
(3, '10224560a2774627e2.jpg', 'adsafdagf'),
(4, 'bg.jpg', 'adsafdagf');

-- --------------------------------------------------------

--
-- Table structure for table `nav_menu`
--

CREATE TABLE IF NOT EXISTS `nav_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL DEFAULT '0',
  `menu` varchar(128) NOT NULL,
  `link` varchar(200) NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `nav_menu`
--

INSERT INTO `nav_menu` (`id_menu`, `id_parent`, `menu`, `link`, `is_active`) VALUES
(1, 0, 'Home', '', 1),
(2, 0, 'About Us', 'content/info/AboutUs', 1),
(3, 0, 'TK/Play Group', 'content/info/tk-playgroup', 1),
(4, 0, 'Program Learning', '#', 0),
(5, 0, 'Articles', 'news/articles', 1),
(6, 0, 'Gallery', 'gallery', 1),
(7, 0, 'Contact & Pendaftaran', 'contact', 0),
(20, 4, 'English Course', 'content/info/english-course-for-children', 0),
(21, 4, 'Kursus Matematika', 'content/info/kursus-matematika', 0),
(22, 4, 'Ca-Lis-Tung', 'content/info/ca-lis-tung', 1),
(23, 4, 'Kursus Menggambar', 'content/info/kursus-menggambar', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran`
--

CREATE TABLE IF NOT EXISTS `pendaftaran` (
  `id_pendaftaran` int(11) NOT NULL AUTO_INCREMENT,
  `nama_daftar` varchar(100) NOT NULL,
  `nama_ortu` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `nama_anak` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `umur` int(11) NOT NULL,
  `kelas_tujuan` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  PRIMARY KEY (`id_pendaftaran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `programs`
--

CREATE TABLE IF NOT EXISTS `programs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_program` varchar(200) NOT NULL,
  `path` varchar(200) NOT NULL,
  `pendaftaran` int(11) NOT NULL,
  `uangkursus_perbulan` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `programs`
--

INSERT INTO `programs` (`id`, `nama_program`, `path`, `pendaftaran`, `uangkursus_perbulan`) VALUES
(1, 'Kursus Matematika', 'kursus-matematika', 200000, 250000),
(2, 'Kursus Menggambar', 'kursus-menggambar', 200000, 250000),
(3, 'Ca-Lis-Tung', 'kursus-calistung', 200000, 250000),
(4, 'English Course for Children', 'english-for-children', 200000, 500000);

-- --------------------------------------------------------

--
-- Table structure for table `programs_keunggulan`
--

CREATE TABLE IF NOT EXISTS `programs_keunggulan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `ket_keunggulan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `programs_keunggulan`
--

INSERT INTO `programs_keunggulan` (`id`, `program_id`, `ket_keunggulan`) VALUES
(1, 1, 'Meteri mengikuti kurikulum nasional'),
(2, 1, 'Bagi anak kelas 6 di beri latihan UAN'),
(3, 1, 'Satu kelas 10 orang'),
(4, 1, 'Diajar oleh guru lulusan FKIP yang berkualitas'),
(27, 2, 'Diajar oleh pakar menggambar '),
(28, 2, 'Anak-anak belajar mewarnai dan menggambar di tempat yang nyaman dan dekat dengan lingkungan '),
(29, 3, 'Anak  - anak di ajar membaca,menulis dan berhitung dengan metode yang mudah di mengerti oleh anak '),
(30, 3, 'Dalam satu kelas 10 orang '),
(31, 3, 'Guru  yang lebih mahir dan instruktur berpengalaman di bidangnya '),
(32, 3, 'Materi  yang menarik '),
(33, 3, 'Untuk menguji kemampuan anak membaca maka  anak di uji untuk  menjawab  pertanyaan. '),
(34, 4, 'Lokasi yang aman untuk anak anak '),
(35, 4, 'Full Ac'),
(36, 4, 'Free internet '),
(37, 4, 'Sistem belajar yang kreatif dan menyenangkan '),
(38, 4, 'Setiap kelas tersedia Tape recorder '),
(39, 4, 'Jumlah murid dalam 1 kelas 10 orang '),
(40, 4, 'Buku yang menarik dan di sesuaikan dengan usia anak'),
(41, 4, 'Adanya kelas survey untuk anak-anak ,sehingga anak di didik untuk berani  berbahasa  inggris '),
(42, 4, 'Di ajar oleh guru-guru yang berpengalaman'),
(43, 4, 'Di akhir level di adakan evaluasi anak di bidang : Speaking,reading ,structure,vocabulary,writing and listening');

-- --------------------------------------------------------

--
-- Table structure for table `programs_syarat`
--

CREATE TABLE IF NOT EXISTS `programs_syarat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `ket_syarat` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `programs_syarat`
--

INSERT INTO `programs_syarat` (`id`, `program_id`, `ket_syarat`) VALUES
(1, 1, 'Kelas 1 sampai kelas 6 SD'),
(2, 1, 'Melunasi biaya kursus tepat pada waktunya'),
(3, 1, 'Melampirkan pas poto 3x4 : 1 lembar'),
(26, 2, 'Berusia mulai umur 3 tahun'),
(27, 2, 'Melampirkan pas poto 3x4 :1lbr'),
(28, 2, 'Mengisi formulir dengan lengkap '),
(29, 2, 'Uang kursus di bayar di muka'),
(30, 3, 'Berusia minimal 4,5 tahun  - 7 tahun'),
(31, 3, 'Melampirkan pas photo 3x4  = 1 lbr    '),
(32, 3, 'Membawa peralatan alat tulis'),
(33, 4, 'Berusia 3 - 15 Tahun '),
(34, 4, 'Melampirkan pas foto 3 x 4 = 1 lbr'),
(35, 4, 'Mengisi formulir pendaftaran '),
(36, 4, 'Mengikuti ujian kenaikan level '),
(37, 4, 'Melunasi biaya daftar dan biaya kursus');

-- --------------------------------------------------------

--
-- Table structure for table `program_jadwal`
--

CREATE TABLE IF NOT EXISTS `program_jadwal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `hari_jadwal` varchar(100) NOT NULL,
  `jam` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `program_jadwal`
--

INSERT INTO `program_jadwal` (`id`, `program_id`, `hari_jadwal`, `jam`) VALUES
(1, 1, 'Senin & Rabu', '14.30-15.45 15.45-17.00 17.00-18.15'),
(2, 1, 'Selasa & Jumat', '14.30-15.45 15.45-17.00 17.00-18.15'),
(3, 1, 'Kamis & Sabtu', '14.30-15.45 15.45-17.00 17.00-18.15'),
(25, 2, 'Senin & Rabu', '14.30-15.45 15.45-17.00 17.00-18.15'),
(26, 2, 'Kamis & Sabtu', '14.30-15.45 15.45-17.00 17.00-18.15'),
(27, 3, 'SELASA & JUMAT', '15.45-17.00 17.00-18.15'),
(28, 3, 'SENIN &  RABU', '15.45-17.00 17.00-18.15'),
(29, 3, 'KAMIS  & SABTU', '17.00-18.15'),
(30, 4, 'Senin & Rabu', '14.30-15.45 15.45-17.00 17.00-18.15'),
(31, 4, 'Selasa & Jumat', '14.30-15.45 15.45-17.00 17.00-18.15'),
(32, 4, 'Kamis & Sabtu', '14.30-15.45 15.45-17.00 17.00-18.15');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `background` varchar(128) NOT NULL,
  `contact_us` text NOT NULL,
  `youtube_link` varchar(128) NOT NULL,
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `background`, `contact_us`, `youtube_link`) VALUES
(1, 'bg.jpg', '<p>okeee</p>\r\n', '1U2DKKqxHgE');

-- --------------------------------------------------------

--
-- Table structure for table `shoutbox`
--

CREATE TABLE IF NOT EXISTS `shoutbox` (
  `shout_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(64) NOT NULL,
  `pesan` text NOT NULL,
  `waktu` datetime NOT NULL,
  PRIMARY KEY (`shout_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `shoutbox`
--

INSERT INTO `shoutbox` (`shout_id`, `nama`, `pesan`, `waktu`) VALUES
(5, 'Admin', 'Hello World', '2015-10-07 08:05:18'),
(6, 'Test', 'test123', '2015-10-07 08:06:27');

-- --------------------------------------------------------

--
-- Table structure for table `slideshow`
--

CREATE TABLE IF NOT EXISTS `slideshow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `slideshow`
--

INSERT INTO `slideshow` (`id`, `file_path`, `keterangan`) VALUES
(1, '02.jpg', 'Joyfulkids'),
(3, '04.jpg', 'Joyfulkids'),
(4, '05.jpg', 'Joyfulkids'),
(5, '1871056054813bb5c1.jpg', 'Test 1'),
(6, '2744560a1f21cc593.jpg', 'asdasda');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
