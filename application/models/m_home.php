<?php 
class m_home extends CI_Model{
	public function get_slideshow(){
		$query = $this->db->query("SELECT * FROM slideshow");
		return $query->result_array();
	}

	public function get_homeitem(){
		$query = $this->db->query("SELECT * FROM home_menu_item");
		return $query->result_array();
	}

	public function get_menu(){
		$query = $this->db->query("SELECT * FROM nav_menu WHERE id_parent = 0 ORDER BY id_menu ASC");
		return $query->result_array();
	}

	public function get_submenu(){
		$query = $this->db->query("SELECT * FROM nav_menu WHERE id_parent <> 0 ORDER BY id_parent ASC");
		return $query->result_array();
	}

	public function get_setting(){
		$query = $this->db->query("SELECT * FROM setting LIMIT 1");
		return $query->row_array();
	}

	public function get_content($id){
		$query = $this->db->query("SELECT * FROM content WHERE path = '$id'");
		return $query->row_array();
	}

	public function get_program($info){
		$query = $this->db->query("SELECT * FROM programs WHERE path = '$info' LIMIT 1");
		return $query->row_array();
	}

	public function get_keunggulan($id){
		$query = $this->db->query("SELECT * FROM programs_keunggulan WHERE program_id = $id ORDER BY id ASC");
		return $query->result_array();
	}

	public function get_syarat($id){
		$query = $this->db->query("SELECT * FROM programs_syarat WHERE program_id = $id ORDER BY id ASC");
		return $query->result_array();
	}

	public function get_jadwal($id){
		$query = $this->db->query("SELECT * FROM program_jadwal WHERE program_id = $id ORDER BY id ASC");
		return $query->result_array();
	}

	public function get_shoubox(){
		$query = $this->db->query("SELECT * FROM shoutbox ORDER BY waktu DESC");
		return $query->result_array();	
	}

	public function save_shout($value){
		$query = $this->db->insert('shoutbox',$value);
    	if ($query) 
    		return true;
    	return false;
	}
}