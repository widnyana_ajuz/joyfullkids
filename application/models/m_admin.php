<?php 
class m_admin extends CI_Model{
	public function get_slideshow(){
		$query = $this->db->query("SELECT * FROM slideshow");
		return $query->result_array();
	}

    public function get_image(){
        $query = $this->db->query("SELECT * FROM image ORDER BY id DESC");
        return $query->result_array();
    }

	public function hapus_slide($id){
		$delete = $this->db->delete('slideshow', array('id'=>$id));

		return $delete;
	}

    public function hapus_image($id){
        $delete = $this->db->delete('image', array('id'=>$id));

        return $delete;
    }

	public function hapus_item($id){
		$delete = $this->db->delete('home_menu_item', array('id'=>$id));

		return $delete;
	}

    public function hapus_content($id){
        $delete = $this->db->delete('content', array('id'=>$id));

        return $delete;
    }

    public function hapus_menu($id){
        $delete = $this->db->delete('nav_menu', array('id_menu'=>$id));

        return $delete;
    }

    public function hapus_program($id){
        $delete = $this->db->delete('programs', array('id'=>$id));
        $delete1 = $this->db->delete('programs_syarat', array('program_id'=>$id));
        $delete2 = $this->db->delete('programs_keunggulan', array('program_id'=>$id));
        $delete3 = $this->db->delete('program_jadwal', array('program_id'=>$id));

        return $delete;
    }

	public function save_slide($value){
		$query = $this->db->insert('slideshow',$value);
    	if ($query) 
    		return true;
    	return false;
	}

    public function save_album($value){
        $query = $this->db->insert('album',$value);
        if ($query) 
            return true;
        return false;
    }

    public function save_album_foto($value){
        $query = $this->db->insert('album_photos',$value);
        if ($query) 
            return true;
        return false;
    }

    public function save_image($value){
        $query = $this->db->insert('image',$value);
        if ($query) 
            return true;
        return false;
    }

	public function get_homeitem(){
		$query = $this->db->query("SELECT * FROM home_menu_item");
		return $query->result_array();
	}

	public function save_item($value){
		$query = $this->db->insert('home_menu_item',$value);
    	if ($query) 
    		return true;
    	return false;
	}

	public function update_item($id, $data){
        $this->db->where('id', $id);
        $update = $this->db->update('home_menu_item', $data);

        if($update)
            return 'true';
        return 'false';
    }

    public function save_article($value){
		$query = $this->db->insert('articles',$value);
    	if ($query) 
    		return true;
    	return false;
	}

	public function update_article($id, $data){
        $this->db->where('id', $id);
        $update = $this->db->update('articles', $data);

        if($update)
            return 'true';
        return 'false';
    }

    public function save_content($value){
        $query = $this->db->insert('content',$value);
        if ($query) 
            return true;
        return false;
    }

    public function update_content($id, $data){
        $this->db->where('id', $id);
        $update = $this->db->update('content', $data);

        if($update)
            return 'true';
        return 'false';
    }

    public function save_menu($value){
        $query = $this->db->insert('nav_menu',$value);
        if ($query) 
            return true;
        return false;
    }

    public function update_menu($id, $data){
        $this->db->where('id_menu', $id);
        $update = $this->db->update('nav_menu', $data);

        if($update)
            return 'true';
        return 'false';
    }

    public function get_content($id){
    	$query = $this->db->query("SELECT * FROM articles WHERE id = '$id'");
		return $query->row_array();
    }

    public function get_contentmenu($id){
        $query = $this->db->query("SELECT * FROM content WHERE id = '$id'");
        return $query->row_array();
    }

    public function get_pendaftar(){
    	$query = $this->db->query("SELECT * FROM pendaftaran");
		return $query->result_array();
    }

    public function update_contact($data){
    	$this->db->where('id_setting', '1');
        $update = $this->db->update('setting', $data);

        if($update)
            return 'true';
        return 'false';
    }

    public function hapus_daftar($id){
    	$delete = $this->db->delete('pendaftaran', array('id_pendaftaran'=>$id));

		return $delete;
    }

    public function get_setting(){
    	$query = $this->db->query("SELECT * FROM setting WHERE id_setting = 1");
		return $query->row_array();
    }

    public function get_allcontent(){
        $query = $this->db->query("SELECT * FROM content");
        return $query->result_array();
    }

    public function get_nav_menu(){
        $query = $this->db->query("SELECT * FROM nav_menu");
        return $query->result_array();
    }

    public function get_allprograms(){
        $query = $this->db->query("SELECT * FROM programs");
        return $query->result_array();
    }

    public function get_idprogram(){
        $query = $this->db->query("SELECT max(id) as id FROM programs ");
        return $query->row_array();
    }
    
    public function save_program($value){
        $query = $this->db->insert('programs',$value);
        if ($query) 
            return true;
        return false;
    }
    
    public function save_program_syarat($value){
        $query = $this->db->insert('programs_syarat',$value);
        if ($query) 
            return true;
        return false;
    }

    public function save_program_keunggulan($value){
        $query = $this->db->insert('programs_keunggulan',$value);
        if ($query) 
            return true;
        return false;
    }

    public function save_program_jadwal($value){
        $query = $this->db->insert('program_jadwal',$value);
        if ($query) 
            return true;
        return false;
    }

    public function tambah_album($value){
        $query = $this->db->insert('album',$value);
        if ($query) 
            return true;
        return false;
    }

    public function get_keunggulan($id){
        $query = $this->db->query("SELECT * FROM programs_keunggulan WHERE program_id = $id ORDER BY id ASC");
        return $query->result_array();
    }

    public function get_syarat($id){
        $query = $this->db->query("SELECT * FROM programs_syarat WHERE program_id = $id ORDER BY id ASC");
        return $query->result_array();
    }

    public function get_jadwal($id){
        $query = $this->db->query("SELECT * FROM program_jadwal WHERE program_id = $id ORDER BY id ASC");
        return $query->result_array();
    }

    public function get_album(){
        $query = $this->db->query("SELECT * FROM album ORDER BY id DESC");
        return $query->result_array();
    }

    public function get_newest_photo($album){
        $query = $this->db->query("SELECT * FROM album_photos WHERE album_id = $album ORDER BY id DESC LIMIT 1");
        $result = $query->row_array();   
        if(!empty($result))
            return $result['file'];
        return "";
    }

    public function get_total_photo($album){
        $query = $this->db->query("SELECT * FROM album_photos WHERE album_id = $album ORDER BY id");
        return $query->num_rows();   
    }

    public function get_albumphoto($id){
        $query = $this->db->query("SELECT * FROM album_photos WHERE album_id = $id ORDER BY id DESC ");
        return $query->result_array();
    }

    public function get_namaalbum($id){
        $query = $this->db->query("SELECT nama_album FROM album WHERE id = $id LIMIT 1 ");
        return $query->row_array();
    }

    public function hapus_photo_album($id){
        $delete = $this->db->delete('album_photos', array('id'=>$id));

        return $delete;              
    }

    public function hapus_photo_on_album($id){
        $delete = $this->db->delete('album_photos', array('album_id'=>$id));

        return $delete;              
    }

    public function hapus_album($id){
        $delete = $this->db->delete('album', array('id'=>$id));

        return $delete;              
    }

    public function hapus_article($id){
        $delete = $this->db->delete('articles', array('id'=>$id));

        return $delete; 
    }
    
}