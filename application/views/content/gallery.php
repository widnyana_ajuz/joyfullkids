      <div class="col-lg-8 col-md-12" style="padding:0px 0px 15px 0px; margin-bottom:0px;" >
        <div class="col-lg-12 col-sm-12 my-content" style="padding:0px 15px 30px 15px; margin-bottom:0px; background:#fff;">
        <h1 style="margin:30px 0px; padding-left:0px;">Gallery</h1>
            <?php
                foreach ($album as $data) {
                    if($data['cover']==""){                                    
                        $url = base_url().'public/img/plus.png';
                    }
                    else{
                        $url = base_url().'public/img/landscape/'.$data['cover'];
                    }

                    echo '
                        <div class="col-lg-4 col-md-6 col-sm-12 cover-album" style="padding:5px 5px 5px 5px;">
                            <a href="'.base_url().'gallery/photo/'.$data['id'].'"> <div class="col-sm-12" style="height:100%; background:url('.$url.') no-repeat; background-size:auto 100%; background-position:center center; padding:210px 0px 0px 0px; border-top: solid 2px #d7d7d7; border-right: solid 2px #d7d7d7; border-left: solid 2px #d7d7d7;">
                            </div></a>
                            <div class="col-sm-12" style="min-height:55px; padding:5px 10px 10px 10px;  background:#fff; opacity: 0.7; border: solid 2px #d7d7d7;">
                                <a href="'.base_url().'gallery/photo/'.$data['id'].'" style=" font-weight:bold;">'.$data['nama_album'].'</a>
                                <br>'.$data['number'].' Foto
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    ';
                }
            ?>
        </div>
      </div>

     <div class="col-lg-4 col-md-12" style="padding:0px 0px 15px 15px;" >
      <div class="col-lg-12 col-sm-12" style="padding:0px; margin-bottom:10px;">
            <?php
              $pertama = rand(0, count($homeitem)-1);

              echo'
              <div class="incontent" style="background:#'.$homeitem[$pertama]['warna_body'].'; height:250px; ">
                <h2 style="background:#'.$homeitem[$pertama]['warna_header'].';">'.$homeitem[$pertama]['judul'].'</h2>
                <p>'.$homeitem[$pertama]['keterangan'].'</p>
                <a href="'.base_url().$homeitem[$pertama]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
              </div>';

              $kedua = rand(0, count($homeitem)-1);
              while($kedua==$pertama){
                $kedua = rand(0, count($homeitem)-1);
              }
              echo'
              <div class="incontent" style="background:#'.$homeitem[$kedua]['warna_body'].'; height:250px; ">
                <h2 style="background:#'.$homeitem[$kedua]['warna_header'].';">'.$homeitem[$kedua]['judul'].'</h2>
                <p>'.$homeitem[$kedua]['keterangan'].'</p>
                <a href="'.base_url().$homeitem[$kedua]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
              </div>';
            ?>
      </div>
    </div>
  </div>
