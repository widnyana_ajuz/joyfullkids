      <div class="col-lg-8 col-md-12" style="padding:0px 0px 15px 0px; margin-bottom:0px;" >
        <div class="col-lg-12 col-sm-12 my-content" style="padding:0px 15px 0px 15px; margin-bottom:0px; background:#fff;">
            <h1>Kursus Matematika</h1>
            
            <h4 style="background:#5b93c4;">Keunggulan</h4>
            <ol>
              <li>Satu kelas 10 orang</li>
              <li>Diajar oleh guru lulusan FKIP yang berkualitas</li>
              <li>Meteri mengikuti kurikulum nasional</li>
              <li>Bagi anak kelas 6 di beri latihan UAN</li>
              <li>Setiap kelas akan diberikan soal-soal latihan jika akan menghadapi ujian</li>
            </ol>

            <h4 style="background:#5b93c4;">Syarat Pendaftaran</h4>
            <ol>
              <li>Kelas 1 sampai kelas 6 SD</li>
              <li>Melampirkan pas poto 3x4 : 1 lembar</li>
              <li>Melunasi biaya kursus tepat pada waktunya</li>
            </ol>

            <h4 style="background:#5b93c4;">Biaya Kursus</h4>
            <table class="table table-hover">
                <tr>
                  <td>Pendaftaran</td>
                  <td>Rp 200.000,00</td>
                </tr>
                <tr>
                  <td>Uang Kursus /bulan</td>
                  <td>Rp 250.000,00</td>
                </tr>
                <tr>
                  <td><b>Total</b></td>
                  <td><b>Rp 450.000,00</b></td>
                </tr>
            </table>

            <h4 style="background:#5b93c4;">Jadwal Kursus</h4>
            <table class="table table-hover">
              <thead>
                <th>Pilihan Hari</th>
                <th>Pilihan Jam</th>
              </thead>
              <tbody>
                <tr>
                  <td>Senin & Rabu</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr>
                <tr>
                  <td>Selasa & Jumat</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr>
                <tr>
                  <td>Kamis & Sabtu</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr>
              </tbody>
            </table>

        </div>
        
      </div>

    <div class="col-lg-4 col-md-12" style="padding:0px 0px 15px 15px;" >
      <div class="col-lg-12 col-sm-12" style="padding:0px; margin-bottom:10px;">
           <div class="incontent" style="background:#c29e70; height:250px; ">
              <h2 style="background:#A25F08;">English Course for Children</h2>
              <p>Lokasi yang aman untuk anak-anak, setiap kelas tersedia tape recorder, jumlah murid dalam 1 kelas 10 orang, buku yang menarik dan disesuaikan dengan usia anak. </p>
              <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>

          <div class="incontent" style="background:#88b9e5; height:250px; ">
            <h2 style="background:#5b93c4;">Kursus Matematika</h2>
            <p>Satu kelas 10 orang, diajar oleh guru lulusan FKIP yang berkualitas, materi mengikuti kurikulum nasional, bagi anak kelas 6 diberi latihan UAN.</p>
            <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>
      </div>
    </div>
  </div>
