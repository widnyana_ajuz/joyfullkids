<!DOCTYPE html>
<html>
<head>
	<title>Joyful Kids</title>
	</style>
</head>
<body style="font-family:Arial, Verdana; background: url(<?=base_url()?>public/img/landscape/<?php echo $setting['background']?>) no-repeat; background-size: auto 100%; background-position: center; background-size: cover;">
    <!-- it works the same with all jquery version from 1.x to 2.x -->
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jquery-latest.min.js"></script>
    <!-- use jssor.slider.mini.js (40KB) instead for release -->
    <!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.slider.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>public/css/bootstrap.min.css"/>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>  
    <script type="javascript/text" src="<?=base_url()?>public/js/bootstrap.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>public/css/myStyles.css"/>  
    <link rel="stylesheet" href="<?=base_url()?>public/css/hover.css"/>
    <link href="<?=base_url()?>public/css/animate.css" rel="stylesheet"/>
    
    <div class="container loginbox" style="max-width:600px;">
    	<div class="col-lg-12 col-sm-12">
    		<h3 class="hlogin">Admin Login</h3>
    		<hr>
            
                <?php
                    if(isset($message)&&$message!="")echo '<div class="col-sm-12" style="background:#FCB3BC; border:#FF0000 2px solid; height:34px; margin-bottom:20px; padding-top:5px;">'.$message.'</div>';
                ?>
    		<form  class="form-horizontal" role="form" action="<?=base_url()?>login/login_validation" method="POST">
    			<div class="form-group">
				    <label class="control-label col-sm-2" >Username:</label>
				    <div class="col-sm-10">
				      <input type="text" class="form-control" id="Username" name="Username" placeholder="Username">
				    </div>
				</div>
				<div class="form-group">
				    <label class="control-label col-sm-2" for="pwd">Password:</label>
				    <div class="col-sm-10">
				      <input type="password" class="form-control" id="Password" name="Password" placeholder="Password">
				    </div>
				</div>
				<hr>
				<div class="form-group">
				    <label class="control-label col-sm-10" for="pwd"></label>
				    <div class="col-sm-2">
				      <button type="submit" class="btn btn-success">Login</button>
				    </div>
				</div>
    		</form>
    	</div>
    </div>

</body>
</html>