      <div class="col-lg-8 col-md-12" style="padding:0px 0px 20px 0px; margin-bottom:0px;" >
        <div class="col-lg-12 col-sm-12 my-content" style="padding:0px 15px 20px 20px; margin-bottom:0px; background:#fff;">
          <h1 style="margin:30px 0px; padding-left:0px;">Contact</h1>
            <?php echo $setting['contact_us']; ?>
        </div>
        <div class="col-lg-12 col-sm-12 my-content" style="padding:0px 15px 20px 15px; margin-bottom:0px; background:#fff;">
          <h1 style="margin:30px 0px; padding-left:10px;">Pendaftaran Online</h1>

          <form  class="form-horizontal" role="form" action="<?=base_url()?>contact/send_contact" method="POST">
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Nama Pendaftar</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="nama_daftar" placeholder="Nama Pendaftaran" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Nama Orang Tua</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="nama_ortu" placeholder="Nama Orang Tua" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Alamat</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="alamat" placeholder="Alamat" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">No. Telpon/ HP</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="no_telp" placeholder="No. Telpon/ HP" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Email</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="email" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Nama Anak</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="nama_anak" placeholder="Email" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Tanggal lahir Anak</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="tgl_lahir" placeholder="Tanggal Lahir Anak" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Umur</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="umur" placeholder="Umur" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Kelas Tujuan</label>
                <div class="col-sm-8">
                  <input class="form-control" id="item_keterangan" name="kelas_tujuan" placeholder="Kelas Tujuan" required>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd">Pesan</label>
                <div class="col-sm-8">
                  <textarea class="form-control" id="item_keterangan" name="pesan" placeholder="Pesan" required> </textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3" for="pwd"></label>
                <div class="col-sm-7">
                  <button type="submit" class="btn btn-success">Simpan</button>
                  <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
          </form>
        </div>
      </div>

    <div class="col-lg-4 col-md-12" style="padding:0px 0px 15px 15px;" >
      <div class="col-lg-12 col-sm-12" style="padding:0px; margin-bottom:10px;">
            <?php
              $pertama = rand(0, count($homeitem)-1);

              echo'
              <div class="incontent" style="background:#'.$homeitem[$pertama]['warna_body'].'; height:250px; ">
                <h2 style="background:#'.$homeitem[$pertama]['warna_header'].';">'.$homeitem[$pertama]['judul'].'</h2>
                <p>'.$homeitem[$pertama]['keterangan'].'</p>
                <a href="'.base_url().$homeitem[$pertama]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
              </div>';

              $kedua = rand(0, count($homeitem)-1);
              while($kedua==$pertama){
                $kedua = rand(0, count($homeitem)-1);
              }
              echo'
              <div class="incontent" style="background:#'.$homeitem[$kedua]['warna_body'].'; height:250px; ">
                <h2 style="background:#'.$homeitem[$kedua]['warna_header'].';">'.$homeitem[$kedua]['judul'].'</h2>
                <p>'.$homeitem[$kedua]['keterangan'].'</p>
                <a href="'.base_url().$homeitem[$kedua]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
              </div>';
            ?>
      </div>
    </div>
  </div>
