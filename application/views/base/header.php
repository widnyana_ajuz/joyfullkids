<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JoyFul Kids</title>
    <link rel="stylesheet" href="<?=base_url()?>public/dist/css/lightbox.css">
</head>
<body style="font-family:Arial, Verdana; background: url(<?=base_url()?>public/img/landscape/<?php echo $setting['background']?>) no-repeat; background-size: auto 100%; background-position: center; background-size: cover;">
    <!-- it works the same with all jquery version from 1.x to 2.x -->
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jquery-latest.min.js"></script>
    <!-- use jssor.slider.mini.js (40KB) instead for release -->
    <!-- jssor.slider.mini.js = (jssor.js + jssor.slider.js) -->
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.slider.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>public/css/bootstrap.min.css"/>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>  
    <script type="javascript/text" src="<?=base_url()?>public/js/bootstrap.js"></script>
    <link rel="stylesheet" href="<?=base_url()?>public/css/myStyles.css"/>  
    <link rel="stylesheet" href="<?=base_url()?>public/css/hover.css"/>
    <link href="<?=base_url()?>public/css/animate.css" rel="stylesheet"/>
    <link href="<?=base_url()?>public/dist/css/lightbox.css" rel="stylesheet"/>

    
    <!-- Jssor Slider Begin -->
    <!-- To move inline styles to css file/block, please specify a class name for each element. --> 
    <div class="container" style="max-width:980px;">
        <header class="col-lg-12" style="padding-left:0px;">
            <div class="col-lg-12 col-sm-12" style="height:100px;"></div>
            <div class="col-lg-6 col-sm-12" style="margin-bottom:20px; padding-left:0px; padding-right:30px;">
                <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>public/img/logo.png" style="width:100%;" alt="Logo Joyful Kids"/></a>
            </div>

            <div class="col-lg-12 col-sm-12" id="menu">
              <ul>
                <?php
                    foreach ($menu as $data) {
                        echo '<li><a href="'.base_url().$data['link'].'" class="hvr-sweep-to-bottom">'.$data['menu'].'</a><ul>';
                        foreach ($submenu as $subdata) {
                            if($subdata['id_parent']==$data['id_menu'])
                                echo '<li><a href="'.base_url().$subdata['link'].'" class="hvr-sweep-to-bottom">'.$subdata['menu'].'</a></li>';
                        }
                        echo '</ul></li>';
                    }
                ?>
                <!-- <li><a href="<?=base_url()?>" class="hvr-sweep-to-bottom">Home</a></li>
                <li><a href="#" class="hvr-sweep-to-bottom">About Us</a></li>
                <li><a href="#" class="hvr-sweep-to-bottom">TK/Play Group</a></li>
                <li><a href="#" class="hvr-sweep-to-bottom">Learning Program</a>
                    <ul>
                        <li><a href="#" class="hvr-sweep-to-bottom">English Course</a></li>
                        <li><a href="#" class="hvr-sweep-to-bottom">Math Course</a></li>
                        <li><a href="#" class="hvr-sweep-to-bottom">Ca-Lis-Tung</a></li>
                    </ul>
                </li>
                <li><a href="<?=base_url()?>news" class="hvr-sweep-to-bottom">Articles</a></li>
                <li><a href="#" class="hvr-sweep-to-bottom">Carrier</a></li>
                <li><a href="<?=base_url()?>contact" class="hvr-sweep-to-bottom">Contact & Pendaftaran</a></li>
              </ul> -->
            </div>
            <div style="clear:both;"></div>
        </header>