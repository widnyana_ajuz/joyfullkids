<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin - Joyful Kids</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=base_url()?>public/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=base_url()?>public/css/myStyles.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=base_url()?>public/css/simple-sidebar.css" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="font-family:Arial, Verdana; background: url(<?=base_url()?>public/img/landscape/<?php echo $setting['background']?>) no-repeat; background-size: auto 100%; background-position: center; background-size: cover;">

    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper" style="background:skyblue; margin-top:-20px">
            <ul class="sidebar-nav myuladmin">
                <li class="sidebar-brand">
                    <a href="#" style="color:#fff;">
                        JOYFUL KIDS
                    </a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin" style="color:#fff;">Halaman Home</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/newsArticles" style="color:#fff;">News & Articles</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/contact" style="color:#fff;">Contact</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/content" style="color:#fff;">Menu & Content</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/gallery" style="color:#fff;">Gallery</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/image" style="color:#fff;">Manage Image</a>
                </li>
                <li>
                    <a href="<?=base_url()?>admin/setting" style="color:#fff;">Setting</a>
                </li>
                <li>
                    <a href="<?=base_url()?>login/logout" style="color:#fff;">Logout</a>
                </li>
                <!-- sisanya CMS -->
            </ul>

            <div style="bottom:0; position:absolute; color:#fff; margin-left:60px; padding-bottom:20px;">2015 &copy; JoyfulKids</div>
        </div>
        <!-- /#sidebar-wrapper -->
        <a href="#menu-toggle" class="button-navigation" id="menu-toggle"></a>