      <div class="col-lg-8 col-md-12" style="padding:0px 0px 15px 0px; margin-bottom:0px;" >
        <div class="col-lg-12 col-sm-12 my-content" style="padding:0px 15px 0px 15px; margin-bottom:0px; background:#fff;">
            <h1><?php echo $program['nama_program'] ?></h1>
            
            <h4 style="background:#5b93c4;">Keunggulan</h4>
            <ol>
              <?php
                  foreach ($keunggulan as $data) {
                    echo '<li>'.$data['ket_keunggulan'].'</li>';
                  }
              ?>
            </ol>

            <h4 style="background:#5b93c4;">Syarat Pendaftaran</h4>
            <ol>
              <?php
                  foreach ($syarat as $data) {
                    echo '<li>'.$data['ket_syarat'].'</li>';
                  }
              ?>
              
            </ol>

            <h4 style="background:#5b93c4;">Biaya Kursus</h4>
            <table class="table table-hover">
                <tr>
                  <td>Pendaftaran</td>
                  <td>Rp <?php echo $program['pendaftaran'] ?></td>
                </tr>
                <tr>
                  <td>Uang Kursus</td>
                  <td>Rp <?php echo $program['uangkursus_perbulan'] ?></td>
                </tr>
                <tr>
                  <td><b>Total</b></td>
                  <td><b>Rp <?php echo intval($program['pendaftaran'])+intval($program['uangkursus_perbulan'])?></b></td>
                </tr>
            </table>

            <h4 style="background:#5b93c4;">Jadwal Kursus</h4>
            <table class="table table-hover">
              <thead>
                <th>Pilihan Hari</th>
                <th>Pilihan Jam</th>
              </thead>
              <tbody>
                <?php
                  foreach ($jadwal as $data) {
                    $pieces = explode(" ", $data['jam']);

                    echo '
                      <tr>
                        <td>'.$data['hari_jadwal'].'</td>
                        <td>';
                    for($i=0; $i<count($pieces); $i++){
                      echo $pieces[$i].'<br>';
                    }
                    echo '</td>
                      </tr>
                    ';
                  }
                ?>
               <!--  <tr>
                  <td>Senin & Rabu</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr>
                <tr>
                  <td>Selasa & Jumat</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr>
                <tr>
                  <td>Kamis & Sabtu</td>
                  <td>14.30 - 15.45 <br> 15.45 - 17.00 <br> 17.00 -  18.15</td>
                </tr> -->
              </tbody>
            </table>

        </div>
        
      </div>

    <div class="col-lg-4 col-md-12" style="padding:0px 0px 15px 15px;" >
      <div class="col-lg-12 col-sm-12" style="padding:0px; margin-bottom:10px;">
           <?php
            $pertama = rand(0, count($homeitem)-1);

            echo'
            <div class="incontent" style="background:#'.$homeitem[$pertama]['warna_body'].'; height:250px; ">
              <h2 style="background:#'.$homeitem[$pertama]['warna_header'].';">'.$homeitem[$pertama]['judul'].'</h2>
              <p>'.$homeitem[$pertama]['keterangan'].'</p>
              <a href="'.base_url().$homeitem[$pertama]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
            </div>';

            $kedua = rand(0, count($homeitem)-1);
            while($kedua==$pertama){
              $kedua = rand(0, count($homeitem)-1);
            }
            echo'
            <div class="incontent" style="background:#'.$homeitem[$kedua]['warna_body'].'; height:250px; ">
              <h2 style="background:#'.$homeitem[$kedua]['warna_header'].';">'.$homeitem[$kedua]['judul'].'</h2>
              <p>'.$homeitem[$kedua]['keterangan'].'</p>
              <a href="'.base_url().$homeitem[$kedua]['link'].'"><button type="button" class="btn btn-danger mybutton">Info</button></a>
            </div>';
          ?>
      </div>
    </div>
  </div>
