        <!-- Page Content -->
        <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Gallery</h3>
                        <hr>
                        <form action="<?=base_url()?>admin/tambah_album" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Tambah Album :</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control" id="album" name="album" placeholder="Nama Album" required>
                                </div>
                                <div class="col-sm-2" style="padding:0px;">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                            
                        </form>
                        <hr>
                    </div>
                    <div class="col-lg-12" id="gallery">
                        <?php
                            foreach ($album as $data) {
                                if($data['cover']==""){                                    
                                    $url = base_url().'public/img/plus.png';
                                }
                                else{
                                    $url = base_url().'public/img/landscape/'.$data['cover'];
                                }

                                echo '
                                    <div class="col-lg-3 col-md-6 col-sm-12 cover-album" style="padding:5px 5px 5px 5px;">
                                        <a style="cursor:pointer; position:absolute; right:0; margin:5px 15px 0px 0px; z-index:1000;" class="hapusImage"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-remove"></span></a>
                                        <a href="'.base_url().'admin/galeryphoto/'.$data['id'].'"> <div class="col-sm-12" style="height:100%; background:url('.$url.') no-repeat; background-size:auto 100%; background-position:center center; padding:210px 0px 0px 0px; border-top: solid 2px #d7d7d7; border-right: solid 2px #d7d7d7; border-left: solid 2px #d7d7d7;">
                                        </div></a>
                                        <div class="col-sm-12" style="min-height:55px; padding:5px 10px 10px 10px;  background:#fff; opacity: 0.7; border: solid 2px #d7d7d7;">
                                            <a href="'.base_url().'admin/galeryphoto/'.$data['id'].'" style=" font-weight:bold;">'.$data['nama_album'].'</a>
                                            <br>'.$data['number'].' Foto
                                        </div>
                                        <div style="clear:both;"></div>
                                    </div>
                                ';
                            }
                        ?>
                    </div>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->


    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('.hapusImage').click(function(){
                var item = {};
                item['id_album'] = $(this).find('.getId').val();
                
                $.ajax({
                    type:'POST',
                    data:item,
                    url:'<?=base_url()?>admin/hapus_album',
                    success:function(data){
                        window.location = '<?=base_url()?>admin/gallery';
                    },error:function(data){
                        console.log(data);
                    }
                })
            })

            $('#reset_item').click(function(){
                $('#status').val('insert');
            })
        })
    </script>

</body>

</html>
