        <!-- Page Content -->
        <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Home - Slideshow</h3>
                        <hr>
                        <form action="<?=base_url()?>admin/upload_slide" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Pilih Gambar :</label>
                                <div class="col-sm-9">
                                  <input type="file" class="form-control" id="gambar" name="gambar" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Keterangan :</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-sm-10" for="pwd"></label>
                                <div class="col-sm-2" style="padding:0px;">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>

                        <table class="table table-hover table-bordered">
                            <thead>
                              <tr>
                                <th width="10">No.</th>
                                <th>File Gambar</th>
                                <th>Keterangan</th>
                                <th width="20">Action</th>
                              </tr>
                            </thead>
                            <tbody id="tbody_slide">
                            <?php
                                $no = 1;
                                foreach ($slideshow as $data) {
                                    echo '
                                        <tr>
                                            <td>'.$no++.'</td>
                                            <td><img src="'.base_url().'public/img/landscape/'.$data['file_path'].'" style="height:150px; width:auto;"></td>
                                            <td>'.$data['keterangan'].'</td>
                                            <td><center><a style="cursor:pointer" class="hapusImage"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a></center></td>
                                        </tr>
                                    ';
                                }
                            ?>                             
                            </tbody>
                          </table>
                    </div>

                    <div class="col-lg-12">
                        <h3>Home - Menu Item</h3>
                        <hr>
                        <form  class="form-horizontal" role="form" action="<?=base_url()?>admin/save_menu" method="POST">
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Judul :</label>
                                <div class="col-sm-9">
                                  <input type="hidden" value="insert" id="status" name="status">
                                  <input type="hidden" value="" id="item_id" name="item_id">
                                  <input type="text" class="form-control" id="item_judul" name="judul" placeholder="Judul">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Path Info :</label>
                                <div class="col-sm-9">
                                  <input type="text" class="form-control" id="item_link" name="link" placeholder="Contoh admin/admin">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Keterangan :</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" id="item_keterangan" name="keterangan" placeholder="Keterangan"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Warna Header :</label>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control" id="warnaheader" name="warna_header" placeholder="Warna Header">
                                </div>
                                <label class="control-label col-sm-3" for="pwd">Warna Body :</label>
                                <div class="col-sm-3">
                                  <input type="text" class="form-control" id="warnabody" name="warna_body" placeholder="Warna Body">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-sm-10" for="pwd"></label>
                                <div class="col-sm-2" style="padding:0px;">
                                  <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                                  <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>

                        </form>
                        <table class="table table-hover table-bordered">
                            <thead>
                              <tr>
                                <th width="10">No.</th>
                                <th>Judul</th>
                                <th>Path Info</th>
                                <th>Keterangan</th>
                                <th>Warna</th>
                                <th width="20">Action</th>
                              </tr>
                            </thead>
                            <tbody id="tbody_item">
                                <?php
                                    $no = 1;
                                    foreach ($homeitem as $data) {
                                        echo'
                                          <tr>
                                            <td>'.$no++.'</td>
                                            <td>'.$data['judul'].'</td>
                                            <td>'.$data['link'].'</td>
                                            <td>'.$data['keterangan'].'</td>
                                            <td width="100">
                                                <span class="warnah" style="display:block; width:20px; height:20px; background:#'.$data['warna_header'].'; padding-left:20px; margin-bottom:10px;">'.$data['warna_header'].'</span>
                                                <span class="warnab" style="display:block; width:20px; height:20px; background:#'.$data['warna_body'].'; padding-left:20px;">'.$data['warna_body'].'</span>
                                            </td>
                                            <td><center>
                                                <a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-edit"></span></a>
                                                <a style="cursor:pointer" class="hapusItem"><input type="hidden" class="getItem" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                            </center></td>
                                          </tr>
                                        ';
                                    }
                                ?>
                            </tbody>
                          </table>
                        
                    </div>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->


    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_slide').on('click','tr td .hapusImage', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var con = confirm('Yakin Ingin Menghapus');

                if(!con)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_slide/'+id,
                    success:function(data){
                        console.log(data);
                        $('#tbody_slide').empty();

                        for(var i = 0; i<data.length; i++){
                            $('#tbody_slide').append(
                                '<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+data[i]['judul']+'</td>'+
                                    '<td>'+data[i]['link']+'</td>'+
                                    '<td>'+data[i]['keterangan']+'</td>'+
                                    '<a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'+data['id']+'"><span class="glyphicon glyphicon-edit"></span></a>'+
                                    '<a style="cursor:pointer" class="hapusItem"><input type="hidden" class="getId" value="'+data['id']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                '</tr>'
                            );
                        }
                    },error:function(data){
                        console.log(data);
                    }
                })
            })
            
            $('#tbody_item').on('click','tr td .hapusItem', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var con = confirm('Yakin Ingin Menghapus');

                if(!con)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_item/'+id,
                    success:function(data){
                        console.log(data);
                        $('#tbody_item').empty();

                        for(var i = 0; i<data.length; i++){
                            $('#tbody_item').append(
                                '<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td>'+data[i]['judul']+'</td>'+
                                    '<td>'+data[i]['link']+'</td>'+
                                    '<td>'+data[i]['keterangan']+'</td>'+
                                    '<td width="100">'+
                                        '<span class="warnah" style="display:block; width:20px; height:20px; background:#'+data[i]['warna_header']+'; padding-left:20px; margin-bottom:10px;">'+data[i]['warna_header']+'</span>'+
                                        '<span class="warnab" style="display:block; width:20px; height:20px; background:#'+data[i]['warna_body']+'; padding-left:20px;">'+data[i]['warna_body']+'</span>'+
                                    '</td>'+
                                    '<td><center>'+
                                    '<a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-edit"></span></a>'+
                                        '<a style="cursor:pointer" class="hapusItem"><input type="hidden" class="getItem" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                    '</center></td>'+
                                '</tr>'
                            );
                        }
                    },error:function(data){
                        console.log(data);
                    }
                })
            })

            $('#tbody_item').on('click','tr td .editItem', function(){
                var id = $(this).closest('tr').find('.getItem').val();
                var judul = $(this).closest('tr').find('td').eq(1).text();;
                var link = $(this).closest('tr').find('td').eq(2).text();
                var keterangan = $(this).closest('tr').find('td').eq(3).text();
                var warnah = $(this).closest('tr').find('.warnah').text();
                var warnab = $(this).closest('tr').find('.warnab').text();

                $('#item_id').val(id);
                $('#item_judul').val(judul);
                $('#item_link').val(link);
                $('#item_keterangan').val(keterangan);
                $('#warnaheader').val(warnah);
                $('#warnabody').val(warnab);
                $('#status').val('update');

                $('#item_judul').focus();
            })

            $('#reset_item').click(function(){
                $('#status').val('insert');
            })
        })
    </script>

</body>

</html>
