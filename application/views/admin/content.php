    <!-- Page Content -->
    <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
    <form class="form-horizontal"  action="<?=base_url()?>admin/save_content" method="POST">
        <main>
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">Content</h3>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Judul</label>
                <div class="col-sm-5">
                    <input type="hidden" id="c_id" name="c_id" value="">
                    <input type="hidden" value="insert" id="status" name="status">
                    <input type="text" class="form-control" id="c_judul" name="c_judul" required placeholder="Judul Content">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Path</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="c_path" name="c_path" required placeholder="Spasi dipisah dengan '-' atau disambung">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Link</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="c_link" name="c_link" placeholder="Generate Link">
                </div>
            </div>
            <div class="adjoined-bottom">
                <div class="grid-container">
                    <div class="grid-width-100">
                        <textarea name="editor1" id="editor1" rows="10" cols="80" required>
                            
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="form-group" style="margin-top:20px;">
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
        </main>
    </form>
    
    <div class="container-fluid">
        <div class="row">
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Judul Content</th>
                        <th>Path</th>
                        <th>Link</th>
                        <th width="20">Action</th>
                      </tr>
                    </thead>
                    <tbody id="tbody_content">
                        <?php
                            $no = 1;
                            foreach ($records as $data) {
                                echo '
                                    <tr>
                                        <td>'.$data['judul'].'</td>
                                        <td>'.$data['path'].'</td>
                                        <td>'.base_url().'content/info/'.$data['path'].'</td>
                                        <td><center>
                                            <a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a style="cursor:pointer" class="hapusContent"><input type="hidden" class="gethapusId" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                        </center></td>
                                    </tr>
                                ';
                            }
                        ?> 
                    </tbody>
                </table>

                <div class="adjoined-top" style="margin-top:50px;">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">Menu</h3>
                    </div>
                </div>
                <hr>
                <form class="form-horizontal"  action="<?=base_url()?>admin/save_navmenu" method="POST">
                    <div class="form-group">
                        <label class="control-label col-sm-1" required>Urutan</label>
                        <div class="col-sm-5">
                            <input type="hidden" value="insert" id="m_status" name="m_status">
                            <input type="hidden" name="old_id" id="old_id">
                            <input type="text" class="form-control" id="m_id" name="m_id" required placeholder="Urutan Tidak ada yang boleh sama">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-1" required>Parent</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="p_id" name="p_id" required placeholder="Sub Menu dari urutan ke berapa">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-1" required>Judul</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="m_judul" name="m_judul" required placeholder="Judul Menu">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-1" required>Path</label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="m_path" name="m_path" placeholder="Spasi dipisah dengan '-' atau disambung">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-1" required>Link</label>
                        <div class="col-sm-5">
                          <input type="text" class="form-control" id="m_link" name="m_link" placeholder="Generate Link">
                        </div>
                    </div>
                    <div class="form-group" style="margin-top:20px;">
                        <label class="control-label col-sm-1" required></label>
                        <div class="col-sm-5">
                            <button type="submit" class="btn btn-success">Simpan</button>
                            <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                        </div>
                    </div>
                </form>
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Urutan</th>
                        <th>Parent</th>
                        <th>Judul</th>
                        <th>Path</th>
                        <th width="20">Action</th>
                      </tr>
                    </thead>
                    <tbody id="tbody_menu">
                        <?php
                            $no = 1;
                            foreach ($nav_menu as $data) {
                                echo '
                                    <tr>
                                        <td>'.$data['id_menu'].'</td>
                                        <td>'.$data['id_parent'].'</td>
                                        <td>'.$data['menu'].'</td>
                                        <td>'.$data['link'].'</td>
                                        <td><center>
                                            <a style="cursor:pointer" class="editmenu"><input type="hidden" class="getId" value="'.$data['id_menu'].'"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a style="cursor:pointer" class="hapusmenu"><input type="hidden" class="gethapusId" value="'.$data['id_menu'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                        </center></td>
                                    </tr>
                                ';
                            }
                        ?> 
                    </tbody>
                </table>

            </div>


        </div>
    </div>        
    </div>

    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url()?>public/ckeditor/samples/js/sample.js"></script>
    <script>
        CKEDITOR.replace( 'editor1' );

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_menu').on('click','tr td .editmenu', function(){
                var id = $(this).closest('tr').find('td').eq(0).text();
                var p_id = $(this).closest('tr').find('td').eq(1).text();
                var judul = $(this).closest('tr').find('td').eq(2).text();
                var path = $(this).closest('tr').find('td').eq(3).text();

                $('#m_id').val(id);
                $('#old_id').val(id);
                $('#p_id').val(p_id);
                $('#m_judul').val(judul);
                $('#m_path').val(path);
                $('#m_link').val('<?=base_url()?>content/'+path);
                $('#m_status').val('update');

                $('#m_judul').focus();
            })

            $('#m_link').focus(function(){
                var path = $('#m_path').val();

                $('#m_link').val('<?=base_url()?>'+path);
            })

            $('#c_link').focus(function(){
                var path = $('#c_path').val();

                $('#c_link').val('<?=base_url()?>content/info/'+path);
            })

            $('#tbody_content').on('click','tr td .hapusContent', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var cnfrm = confirm('Yakin Ingin Menghapus?');
                if(!cnfrm)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_content/'+id,
                    success:function(data){
                        $('#tbody_content').empty();

                        for(var i=0; i<data.length;i++){
                            $('#tbody_content').append(
                                '<tr>'+
                                    '<td>'+data[i]['judul']+'</td>'+
                                    '<td>'+data[i]['path']+'</td>'+
                                    '<td>'+data[i]['path']+'</td>'+
                                    '<td><center>'+
                                        '<a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-edit"></span></a>'+
                                        '<a style="cursor:pointer" class="hapusContent"><input type="hidden" class="gethapusId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                    '</center></td>'+
                                '</tr>'
                            );
                        }

                        if(data.length==0){
                            $('#tbody_content').append('<tr><td colspan="6"><center>Tidak Ada Pendaftaran<center></td></tr>');
                        }
                    }
                });
            })

            $('#tbody_content').on('click','tr td .editItem', function(){
                var id = $(this).closest('tr').find('.getId').val();
                var judul = $(this).closest('tr').find('td').eq(0).text();;
                var path = $(this).closest('tr').find('td').eq(1).text();

                $('#c_id').val(id);
                $('#c_judul').val(judul);
                $('#c_path').val(path);
                $('#c_link').val('<?=base_url()?>content/'+path);
                $('#status').val('update');

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_contentmenu/'+id,
                    success:function(data){
                        console.log(data);
                        CKEDITOR.instances['editor1'].setData(data['content']);
                    },error:function(data){
                        console.log(data);
                    }
                })
                
                $('#c_judul').focus();
            })

            $('#tbody_menu').on('click','tr td .hapusmenu', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var cnfrm = confirm('Yakin Ingin Menghapus?');
                if(!cnfrm)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_menu/'+id,
                    success:function(data){
                        $('#tbody_menu').empty();

                        for(var i=0; i<data.length;i++){
                            $('#tbody_menu').append(
                                '<tr>'+
                                    '<td>'+data[i]['id_menu']+'</td>'+
                                    '<td>'+data[i]['id_parent']+'</td>'+
                                    '<td>'+data[i]['menu']+'</td>'+
                                    '<td>'+data[i]['link']+'</td>'+
                                    '<td><center>'+
                                        '<a style="cursor:pointer" class="editmenu"><input type="hidden" class="getId" value="'+data[i]['id_menu']+'"><span class="glyphicon glyphicon-edit"></span></a>'+
                                        '<a style="cursor:pointer" class="hapusmenu"><input type="hidden" class="gethapusId" value="'+data[i]['id_menu']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                    '</center></td>'+
                                '</tr>'
                            );
                        }

                        if(data.length==0){
                            $('#tbody_menu').append('<tr><td colspan="6"><center>Tidak Ada Pendaftaran<center></td></tr>');
                        }
                    },error:function(data){
                        console.log(data);
                    }
                });
            })
        })
    </script>

</body>

</html>
