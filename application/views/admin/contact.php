    <!-- Page Content -->
    <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
    <form class="form-horizontal"  action="<?=base_url()?>admin/save_contact" method="POST">
        <main>
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">Contact Us</h3>
                    </div>
                </div>
            </div>
            <hr>
            <div class="adjoined-bottom">
                <div class="grid-container">
                    <div class="grid-width-100">
                        <textarea name="editor1" id="editor1" rows="10" cols="80">
                            <?php echo $setting['contact_us']; ?>
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="form-group" style="margin-top:20px;">
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
        </main>
    </form>
    
    <div class="container-fluid">
        <div class="row">
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">Pendaftaran</h3>
                        <hr>
                    </div>
                </div>
            </div>
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Nama Anak</th>
                        <th>Nama Ortu</th>
                        <th>Contact</th>
                        <th>Kelas tujuan</th>
                        <th>pesan</th>
                        <th width="20">Action</th>
                      </tr>
                    </thead>
                    <tbody id="tbody_pendaftar">
                        <?php
                            $no = 1;
                            foreach ($pendaftar as $data) {
                                echo '
                                    <tr>
                                        <td>'.$data['nama_daftar'].'</td>
                                        <td>'.$data['nama_ortu'].'</td>
                                        <td>'.$data['no_telp'].'<br>'.$data['alamat'].'</td>
                                        <td>'.$data['kelas_tujuan'].'</td>
                                        <td>'.$data['pesan'].'</td>
                                        <td><center>
                                            <a style="cursor:pointer" class="hapusDaftar"><input type="hidden" class="getId" value="'.$data['id_pendaftaran'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                        </center></td>
                                    </tr>
                                ';
                            }

                            if(empty($pendaftar))
                                echo '<tr><td colspan="6"><center>Tidak Ada Pendaftaran<center></td></tr>';
                        ?> 
                    </tbody>
                </table>
                <?php //echo $links;?>
        </div>
    </div>        
    </div>

    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url()?>public/ckeditor/samples/js/sample.js"></script>
    <script>
        CKEDITOR.replace( 'editor1' );

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_pendaftar').on('click','tr td .hapusDaftar', function(){
                var id = $(this).find('.getId').val();

                var cnfrm = confirm('Yakin Ingin Menghapus?');
                if(!cnfrm)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_daftar/'+id,
                    success:function(data){
                        $('#tbody_pendaftar').empty();

                        for(var i=0; i<data.length;i++){
                            $('#tbody_pendaftar]').append(
                                '<tr>'+
                                    '<td>'+data[i]['nama_daftar']+'</td>'+
                                    '<td>'+data[i]['nama_ortu']+'</td>'+
                                    '<td>'+data[i]['no_telp']+'<br>'+data[i]['alamat']+'</td>'+
                                    '<td>'+data[i]['kelas_tujuan']+'</td>'+
                                    '<td>'+data[i]['pesan']+'</td>'+
                                    '<td><center>'+
                                        '<a style="cursor:pointer" class="hapusDaftar"><input type="hidden" class="getId" value="'+data[i]['id_pendaftaran']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                    '</center></td>'+
                                '</tr>'
                            );
                        }

                        if(data.length==0){
                            $('#tbody_pendaftar]').append('<tr><td colspan="6"><center>Tidak Ada Pendaftaran<center></td></tr>');
                        }
                    }
                });
            })
        })
    </script>

</body>

</html>
