    <!-- Page Content -->
    <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
    <form class="form-horizontal"  id="submit_program" method="POST">
        <main>
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">Learning Program</h3>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-2" >Nama Program</label>
                <div class="col-sm-5">
                    <input type="hidden" id="p_id" name="p_id" value="">
                    <input type="hidden" value="insert" id="status" name="status">
                    <input type="text" class="form-control" required id="p_judul" name="p_judul" required placeholder="Judul Content">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Path</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" required id="p_path" name="p_path" required placeholder="Spasi dipisah dengan '-' atau disambung">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Link</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" required id="p_link" name="p_link" placeholder="Generate Link">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Pendaftaran (Rp)</label>
                <div class="col-sm-5">
                  <input type="number" class="form-control" required id="p_daftar" name="p_daftar" placeholder="Pendaftaran">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" >Kursus Per Bulan (Rp)</label>
                <div class="col-sm-5">
                  <input type="number" class="form-control" required id="p_bulan" name="p_bulan" placeholder="Kursus Per Bulan">
                </div>
            </div>
            
            <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2" ></label>
                    <div class="col-sm-7">
                      <a style="cursor:pointer;" id="tambahsyarat">Tambah Syarat</a>
                    </div>
                </div>
            <div id="syaratbos">
                <div class="form-group">
                    <label class="control-label col-sm-2" >Syarat 1</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" required id="syarat1" name="p_link" placeholder="Syarat">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" >Syarat 2</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" required id="syarat2" name="p_syarat1" placeholder="Syarat">
                    </div>
                </div>
            </div>

            <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2" ></label>
                    <div class="col-sm-7">
                      <a style="cursor:pointer;" id="tambahkeunggulan">Tambah Keunggulan</a>
                    </div>
                </div>
            <div id="keunggulanbos">
                <div class="form-group">
                    <label class="control-label col-sm-2" >Keunggulan 1</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" required id="keunggulan1" name="p_link" placeholder="Keunggulan">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-2" >Keunggulan 2</label>
                    <div class="col-sm-7">
                      <input type="text" class="form-control" required id="keunggulan2" name="p_syarat1" placeholder="Keunggulan">
                    </div>
                </div>
            </div>

            <hr>
                <div class="form-group">
                    <label class="control-label col-sm-2" ></label>
                    <div class="col-sm-7">
                      <a style="cursor:pointer;" id="tambahharijam">Tambah Jadwal</a>
                    </div>
                </div>
            <div id="harijam">
                <div class="form-group">
                    <label class="control-label col-sm-2" required>Hari & Jam 1</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="hari1" name="p_link" placeholder="Hari">
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="jam1" name="p_syarat1" placeholder="Jam (Gunakan Spasi Ex: 11.00-12.00 14.00-15.00)">
                    </div>
                </div>
            </div>

            <div class="form-group" style="margin-top:20px;">
                <label class="control-label col-sm-2" required></label>
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
        </main>
    </form>
    
    <div class="container-fluid">
        <div class="row">
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Nama Program</th>
                        <th>Path</th>
                        <th>Link</th>
                        <th>Pendaftaran</th>
                        <th width="20">Action</th>
                      </tr>
                    </thead>
                    <tbody id="tbody_content">
                        <?php
                            $no = 1;
                            foreach ($records as $data) {
                                echo '
                                    <tr>
                                        <td>'.$data['nama_program'].'</td>
                                        <td>'.$data['path'].'</td>
                                        <td>'.base_url().'content/'.$data['path'].'</td>
                                        <td align="right">Rp '.$data['pendaftaran'].'</td>
                                        <td><center>
                                            <a style="cursor:pointer" class="editItem"><input type="hidden" class="getkursus" value="'.$data['uangkursus_perbulan'].'"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a style="cursor:pointer" class="hapusContent"><input type="hidden" class="gethapusId" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                        </center></td>
                                    </tr>
                                ';
                            }
                        ?> 
                    </tbody>
                </table>

            </div>


        </div>
    </div>        
    </div>

    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url()?>public/ckeditor/samples/js/sample.js"></script>
    <script>

        $(window).ready(function(){
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });

            var s = 2;
            var h = 1;
            var k = 2;

            $('#tambahsyarat').click(function(){
                s++;
                $('#syaratbos').append(
                    '<div class="form-group">'+
                        '<label class="control-label col-sm-2" >Syarat '+s+'</label>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control" required id="syarat'+s+'" name="p_link" placeholder="Syarat">'+
                    '</div></div>'
                );
            })

            $('#tambahharijam').click(function(){
                h++;
                $('#harijam').append(
                    '<div class="form-group">'+
                        '<label class="control-label col-sm-2" required>Hari & Jam '+h+'</label>'+
                        '<div class="col-sm-4">'+
                            '<input type="text" class="form-control" id="hari'+h+'" name="p_link" placeholder="Hari">'+
                        '</div>'+
                        '<div class="col-sm-4">'+
                            '<input type="text" class="form-control" id="jam'+h+'" name="p_syarat1" placeholder="Jam (Gunakan Spasi Ex: 11.00-12.00 14.00-15.00)">'+
                    '</div></div>'
                );
            })

            $('#tambahkeunggulan').click(function(){
                k++;
                $('#keunggulanbos').append(
                    '<div class="form-group">'+
                        '<label class="control-label col-sm-2" >Keunggulan '+k+'</label>'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control" requisred id="keunggulan'+k+'" name="p_link" placeholder="Syarat">'+
                    '</div></div>'
                );
            })

            $('#submit_program').submit(function(e){
                e.preventDefault();
                var item = {};
                item[1] = {};

                item[1]['nama_program'] = $('#p_judul').val();
                item[1]['path'] = $('#p_path').val();
                item[1]['pendaftaran'] = $('#p_daftar').val();
                item[1]['uangkursus_perbulan'] = $('#p_bulan').val();

                if($('#status').val()=='update'){
                    var id = $('#p_id').val();
                    var data = hapus_program(id);
                }

                $.ajax({
                    type:'POST',
                    data:item,
                    url:'<?=base_url()?>admin/save_program',
                    success:function(data){
                        console.log(data);

                        for(var i = 1; i<=s; i++){
                            var syarat = {};
                            syarat[1] = {};

                            var id = '#syarat'+i;
                            syarat[1]['program_id'] = data;
                            syarat[1]['ket_syarat'] = $(id).val();

                            $.ajax({
                                type:'POST',
                                data:syarat,
                                url:'<?=base_url()?>admin/save_program_syarat',
                                success:function(data){
                                    console.log(data);
                                },error:function(data){
                                    console.log(data);
                                }
                            })
                        }

                        for(var i = 1; i<=k; i++){
                            var syarat = {};
                            syarat[1] = {};

                            var id = '#keunggulan'+i;
                            syarat[1]['program_id'] = data;
                            syarat[1]['ket_keunggulan'] = $(id).val();

                            $.ajax({
                                type:'POST',
                                data:syarat,
                                url:'<?=base_url()?>admin/save_program_keunggulan',
                                success:function(data){
                                    console.log(data);
                                },error:function(data){
                                    console.log(data);
                                }
                            })
                        }

                        for(var i = 1; i<=h; i++){
                            var syarat = {};
                            syarat[1] = {};

                            var id = '#hari'+i;
                            var jam = '#jam'+i;
                            syarat[1]['program_id'] = data;
                            syarat[1]['hari_jadwal'] = $(id).val();
                            syarat[1]['jam'] = $(jam).val();

                            $.ajax({
                                type:'POST',
                                data:syarat,
                                url:'<?=base_url()?>admin/save_program_jadwal',
                                success:function(data){
                                    console.log(data);
                                    window.location.href="<?=base_url()?>admin/program";
                                },error:function(data){
                                    console.log(data);
                                }
                            })
                        }

                    },error:function(data){
                        console.log(data);
                    }
                })//.promise().done(function(){window.location.href="<?=base_url()?>admin/program";})
            }); 

            $('#tbody_content').on('click','tr td .editItem', function(){
                var id = $(this).closest('tr').find('.getId').val();
                var kursus = $(this).closest('tr').find('.getkursus').val();
                var judul = $(this).closest('tr').find('td').eq(0).text();
                var path = $(this).closest('tr').find('td').eq(1).text();
                var pendaftaran = $(this).closest('tr').find('td').eq(3).text();
                var daftar = pendaftaran.split(' ');

                $('#p_id').val(id);
                $('#p_judul').val(judul);
                $('#p_path').val(path);
                $('#p_link').val('<?=base_url()?>content/'+path);
                $('#p_daftar').val(daftar[1]);
                $('#p_bulan').val(kursus);
                $('#status').val('update');

                $('#p_judul').focus();

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_keunggulan/'+id,
                    success:function(data){
                        $('#keunggulanbos').empty();

                        for(var i=0; i<data.length; i++){
                            $('#keunggulanbos').append(
                                '<div class="form-group">'+
                                    '<label class="control-label col-sm-2" >Keunggulan '+(i+1)+'</label>'+
                                    '<div class="col-sm-7">'+
                                      '<input type="text" class="form-control" value="'+data[i]['ket_keunggulan']+'" required id="keunggulan'+(i+1)+'" name="p_link" placeholder="Keunggulan">'+
                                '</div></div>'
                            );
                        }

                        k = data.length;
                    }
                })

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_syarat/'+id,
                    success:function(data){
                        $('#syaratbos').empty();

                        for(var i=0; i<data.length; i++){
                            $('#syaratbos').append(
                                '<div class="form-group">'+
                                    '<label class="control-label col-sm-2" >Syarat '+(i+1)+'</label>'+
                                    '<div class="col-sm-7">'+
                                      '<input type="text" class="form-control" value="'+data[i]['ket_syarat']+'" required id="syarat'+(i+1)+'" name="p_link" placeholder="Keunggulan">'+
                                '</div></div>'
                            );
                        }

                        s = data.length;
                    }
                })

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_jadwal/'+id,
                    success:function(data){
                        $('#harijam').empty();

                        for(var i=0; i<data.length; i++){
                            $('#harijam').append(
                                '<div class="form-group">'+
                                    '<label class="control-label col-sm-2" >Hari & Jam '+(i+1)+'</label>'+
                                    '<div class="col-sm-4">'+
                                        '<input type="text" value="'+data[i]['hari_jadwal']+'" class="form-control" id="hari'+(i+1)+'" name="p_link" placeholder="Hari">'+
                                    '</div>'+
                                    '<div class="col-sm-4">'+
                                        '<input type="text" value="'+data[i]['jam']+'" class="form-control" id="jam'+(i+1)+'" name="p_syarat1" placeholder="Jam (Gunakan Spasi Ex: 11.00-12.00 14.00-15.00)">'+
                                '</div></div>'
                            );
                        }

                        h = data.length;
                    }
                })
            })

            $('#p_path').change(function(){
                var path = $(this).val();

                $('#p_link').val('<?=base_url()?>info/detail/'+path);
            })

            $('#tbody_content').on('click','tr td .hapusContent', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var cnfrm = confirm('Yakin Ingin Menghapus?');
                if(!cnfrm)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_program/'+id,
                    success:function(data){
                        $('#tbody_content').empty();

                        for(var i=0; i<data.length;i++){
                            $('#tbody_content').append(
                                '<tr>'+
                                    '<td>'+data[i]['nama_program']+'</td>'+
                                    '<td>'+data[i]['path']+'</td>'+
                                    '<td><?=base_url()?>content/'+data[i]['path']+'</td>'+
                                    '<td align="right">Rp '+data[i]['pendaftaran']+'</td>'+
                                    '<td><center>'+
                                        '<a style="cursor:pointer" class="editItem"><input type="hidden" class="getkursus" value="'+data[i]['uangkursus_perbulan']+'"><input type="hidden" class="getId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-edit"></span></a>'+
                                        '<a style="cursor:pointer" class="hapusContent"><input type="hidden" class="gethapusId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-trash"></span></a>'+
                                    '</center></td>'+
                                '</tr>'
                            );
                        }

                        if(data.length==0){
                            $('#tbody_content').append('<tr><td colspan="6"><center>Tidak Ada Data<center></td></tr>');
                        }
                    }
                });
            })
        })

        function hapus_program(id){
            alert('ok');

            $.ajax({
                type:'POST',
                url:'<?=base_url()?>admin/hapus_program/'+id,
                success:function(data){
                    return data;
                },error:function(data){
                    console.log(data);
                }
            });

            return false;
        }
    </script>
</body>

</html>
