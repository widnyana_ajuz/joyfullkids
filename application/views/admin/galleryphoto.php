        <!-- Page Content -->
        <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Gallery</h3>
                        <hr>
                        <form action="<?=base_url()?>admin/tambah_album_photo" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Tambah Photo :</label>
                                <div class="col-sm-6">
                                    <input type="hidden" id="album_id" name="album_id" value="<?php echo $album_id;?>">
                                    <input type="text" class="form-control" id="album" name="album" placeholder="Copy nama file dari Manage Image">
                                </div>
                            </div>
                            <center><b><h4 style="margin-top:5px; padding:0px; margin-left:-200px;">Atau</h4></b></center>
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Upload Gambar :</label>
                                <div class="col-sm-6">
                                  <input type="file" class="form-control" id="gambar" name="gambar" placeholder="Upload Gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Keterangan Gambar</label>
                                <div class="col-sm-6">
                                  <input type="text" class="form-control" id="album" name="keterangan" placeholder="Keterangan Gambar">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" ></label>
                                <div class="col-sm-2" >
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                    </div>
                    <div class="col-lg-12" id="gallery">
                        <?php
                            foreach ($album as $data) {
                                echo '
                                    <div class="col-lg-3 col-md-6 col-sm-12 cover-album" style="padding:5px 5px 5px 5px;">
                                        <a style="cursor:pointer; position:absolute; right:0; margin:5px 15px 0px 0px; z-index:1000;" class="hapusImage"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-remove"></span></a>
                                        <img src="'.base_url().'public/img/landscape/'.$data['file'].'" class="img-responsive" alt="Responsive image" style="height:260px;">
                                    </div>
                                ';
                            }
                        ?>
                    </div>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->


    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#reset_item').click(function(){
                $('#status').val('insert');
            })

            $('.hapusImage').click(function(){
                var item = {};
                item['id_photo'] = $(this).find('.getId').val();
                item['id_album'] = $('#album_id').val();
                
                $.ajax({
                    type:'POST',
                    data:item,
                    url:'<?=base_url()?>admin/hapus_photo_album',
                    success:function(data){
                        $('#gallery').empty();

                        for(var i=0; i<data.length;i++){
                            $('#gallery').append(
                                '<div class="col-lg-3 col-md-6 col-sm-12 cover-album" style="padding:5px 5px 5px 5px;">'+
                                    '<a style="cursor:pointer; position:absolute; right:0; margin:5px 15px 0px 0px; z-index:1000;" class="hapusImage"><input type="hidden" class="getId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-remove"></span></a>'+
                                    '<img src="<?=base_url()?>public/img/landscape/'+data[i]['file']+'" class="img-responsive" alt="Responsive image" style="height:260px;">'+
                                '</div>'
                            );
                        }
                        console.log(data);
                    },error:function(data){
                        console.log(data);
                    }
                })
            })
        })
    </script>

</body>

</html>
