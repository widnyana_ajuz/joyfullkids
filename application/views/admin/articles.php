    <!-- Page Content -->
    <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
    <form class="form-horizontal"  action="<?=base_url()?>admin/save_articles" method="POST">
        <main>
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">News And Articles</h3>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Judul</label>
                <div class="col-sm-5">
                    <input type="hidden" id="ar_id">
                    <input type="hidden" value="insert" id="status" name="status">
                    <input type="text" class="form-control" id="ar_judul" name="ar_judul" placeholder="Judul Article atau Berita">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Path</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="ar_path" name="ar_path" placeholder="spasi dipisah dengan '-'">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-1" required>Review Articles</label>
                <div class="col-sm-5">
                  <textarea class="form-control" id="ar_article" name="ar_article" placeholder="Review Articles"></textarea>
                </div>
            </div>
            <div class="adjoined-bottom">
                <div class="grid-container">
                    <div class="grid-width-100">
                        <textarea name="editor1" id="editor1" rows="10" cols="80" required>
                            
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="form-group" style="margin-top:20px;">
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
        </main>
    </form>
    
    <div class="container-fluid">
        <div class="row">
                <table class="table table-hover table-bordered">
                    <thead>
                      <tr>
                        <th>Judul Artikel</th>
                        <th>Path</th>
                        <th width="20">Action</th>
                      </tr>
                    </thead>
                    <tbody id="tbody_article">
                        <?php
                            $no = 1;
                            foreach ($records as $data) {
                                echo '
                                    <tr>
                                        <td>'.$data['judul'].'</td>
                                        <td>'.$data['path'].'</td>
                                        <td><center>
                                            <a style="cursor:pointer" class="editItem"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-edit"></span></a>
                                            <a style="cursor:pointer" class="hapusItem"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a>
                                        </center></td>
                                    </tr>
                                ';
                            }
                        ?> 
                    </tbody>
                </table>
                <?php echo $links;?>
        </div>
    </div>        
    </div>

    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url()?>public/ckeditor/samples/js/sample.js"></script>
    <script>
        CKEDITOR.replace( 'editor1' );

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_article').on('click','tr td .editItem', function(){
                var id = $(this).closest('tr').find('.getId').val();
                var judul = $(this).closest('tr').find('td').eq(0).text();;
                var path = $(this).closest('tr').find('td').eq(1).text();

                $('#ar_id').val(id);
                $('#ar_judul').val(judul);
                $('#ar_path').val(path);
                $('#item_id').val(id);
                $('#status').val('update');

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_content/'+id,
                    success:function(data){
                        console.log(data);
                        $('#ar_article').val(data['review']);
                        CKEDITOR.instances['editor1'].setData(data['content']);
                    },error:function(data){
                        console.log(data);
                    }
                })
                
                $('#ar_judul').focus();
            })

            $('#tbody_article').on('click','tr td .hapusItem', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var c = confirm('Yakin Ingin menghapus Article?');

                if(!c)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_article/'+id,
                    success:function(data){
                         window.location = "<?=base_url()?>admin/newsArticles";
                        //console.log(data);
                    },error:function(data){
                        console.log(data);
                    }
                });
            })
        })
    </script>

</body>

</html>
