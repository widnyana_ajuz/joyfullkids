    <!-- Page Content -->
    <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
    <form class="form-horizontal"  action="<?=base_url()?>admin/save_setting" method="POST">
        <main>
            <div class="adjoined-top">
                <div class="grid-container">
                    <div class="content grid-width-100">
                        <h3 style="margin-bottom:25px;">General Setting</h3>
                    </div>
                </div>
            </div>
            <hr>
            <div class="form-group">
                <label class="control-label col-sm-2" required>Background</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="ar_judul" name="background" placeholder="Copy Nama image dari Manage Gambar Ex: bg.jpg">
                </div>
                <label class=" col-sm-2" required><?php echo $setting['background']?></label>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2" required>Youtube Link</label>
                <div class="col-sm-5">
                  <input type="text" class="form-control" id="ar_path" name="youtube" placeholder="Hanya Kode Watch Saja Ex: l36TTAKsa8w">
                </div>
                <label class=" col-sm-2" required><?php echo $setting['youtube_link']?></label>
            </div>
            <div class="form-group" style="margin-top:20px;">
                <label class="control-label col-sm-2" required></label>
                <div class="col-sm-5">
                    <button type="submit" class="btn btn-success">Simpan</button>
                    <button type="reset" id="reset_item" class="btn btn-danger">Batal</button>
                </div>
            </div>
        </main>
    </form>

    </div>

    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script src="<?=base_url()?>public/ckeditor/ckeditor.js"></script>
    <script src="<?=base_url()?>public/ckeditor/samples/js/sample.js"></script>
    <script>
        CKEDITOR.replace( 'editor1' );

        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_article').on('click','tr td .editItem', function(){
                var id = $(this).closest('tr').find('.getId').val();
                var judul = $(this).closest('tr').find('td').eq(0).text();;
                var path = $(this).closest('tr').find('td').eq(1).text();

                $('#ar_id').val(id);
                $('#ar_judul').val(judul);
                $('#ar_path').val(path);
                $('#item_id').val(id);
                $('#status').val('update');

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/get_content/'+id,
                    success:function(data){
                        console.log(data);
                        $('#ar_article').val(data['review']);
                        CKEDITOR.instances['editor1'].setData(data['content']);
                    },error:function(data){
                        console.log(data);
                    }
                })
                
                $('#ar_judul').focus();
            })
        })
    </script>

</body>

</html>
