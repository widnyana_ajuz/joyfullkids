        <!-- Page Content -->
        <div id="page-content-wrapper" style="background:#fff; margin:20px 20px 20px 20px; max-width:97%;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Manage Image</h3>
                        <hr>
                        <form action="<?=base_url()?>admin/upload_image" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
                            <div class="form-group">
                                <label class="control-label col-sm-2" >Pilih Gambar :</label>
                                <div class="col-sm-9">
                                  <input type="file" class="form-control" id="gambar" name="gambar" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="pwd">Alternatif :</label>
                                <div class="col-sm-9">
                                  <textarea class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan"></textarea>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="control-label col-sm-10" for="pwd"></label>
                                <div class="col-sm-2" style="padding:0px;">
                                  <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>

                        <table class="table table-hover table-bordered">
                            <thead>
                              <tr>
                                <th width="10">No.</th>
                                <th>File Gambar</th>
                                <th>Link</th>
                                <th width="20">Action</th>
                              </tr>
                            </thead>
                            <tbody id="tbody_slide">
                            <?php
                                $no = 1;
                                foreach ($slideshow as $data) {
                                    echo '
                                        <tr>
                                            <td>'.$no++.'</td>
                                            <td><img src="'.base_url().'public/img/landscape/'.$data['file_path'].'" style="height:150px; width:auto;"></td>
                                            <td>'.base_url().'public/img/landscape/'.$data['file_path'].'</td>
                                            <td><center><a style="cursor:pointer" class="hapusImage"><input type="hidden" class="getId" value="'.$data['id'].'"><span class="glyphicon glyphicon-trash"></span></a></center></td>
                                        </tr>
                                    ';
                                }
                            ?>                             
                            </tbody>
                          </table>
                    </div>

                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->


    <script src="<?=base_url()?>public/js/jquery.js"></script>
    <script src="<?=base_url()?>public/js/bootstrap.min.js"></script>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        $(window).ready(function(){
            $('#tbody_slide').on('click','tr td .hapusImage', function(){
                var id = $(this).closest('tr').find('.getId').val();

                var con = confirm('Yakin Ingin Menghapus');

                if(!con)
                    return false;

                $.ajax({
                    type:'POST',
                    url:'<?=base_url()?>admin/hapus_image/'+id,
                    success:function(data){
                        console.log(data);
                        $('#tbody_slide').empty();

                        for(var i = 0; i<data.length; i++){
                            $('#tbody_slide').append(
                                '<tr>'+
                                    '<td>'+(i+1)+'</td>'+
                                    '<td><img src="<?=base_url()?>public/img/landscape/'+data[i]['file_path']+'" style="height:150px; width:auto;"></td>'+
                                    '<td><?=base_url()?>public/img/landscape/'+data[i]['file_path']+'</td>'+
                                    '<td><center><a style="cursor:pointer" class="hapusImage"><input type="hidden" class="getId" value="'+data[i]['id']+'"><span class="glyphicon glyphicon-trash"></span></a></center></td>'+
                                '</tr>'
                            );
                        }
                    },error:function(data){
                        console.log(data);
                    }
                })
            })

            $('#reset_item').click(function(){
                $('#status').val('insert');
            })
        })
    </script>

</body>

</html>
