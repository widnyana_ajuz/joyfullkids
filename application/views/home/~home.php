<!DOCTYPE html>
<html>
<head>
  <title>Joyful Kids</title>
  <!-- <link rel="shortcut icon" type="image/x-icon" href="<?=base_url()?>public/img/header-logo.png"/> -->
  <link rel="stylesheet" href="<?=base_url()?>public/css/bootstrap.min.css"/>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>  
  <script type="javascript/text" src="<?=base_url()?>public/js/bootstrap.js"></script>
  <link rel="stylesheet" href="<?=base_url()?>public/css/myStyles.css"/>  
  <link rel="stylesheet" href="<?=base_url()?>public/css/hover.css"/>
  <link href="<?=base_url()?>public/css/animate.css" rel="stylesheet"/>
</head>
<body>
  <div class="container" style="max-width:960px;">
      <header class="col-lg-12" style="padding-left:0px;">
        <div class="col-lg-12 col-sm-12" style="height:50px;"></div>
        <div class="col-lg-6 col-sm-12" style="margin-bottom:20px; padding-left:0px; padding-right:30px;">
            <a href="<?php echo base_url() ?>"><img src="<?php echo base_url() ?>public/img/logo.png" style="width:100%;" alt="Logo Joyful Kids"/></a>
        </div>

        <div class="col-lg-12 col-sm-12" id="menu">
          <ul>
            <li><a href="#" class="hvr-sweep-to-bottom">Home</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">About Us</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">Play Group</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">Learning Program</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">News & Articles</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">Office</a></li>
            <li><a href="#" class="hvr-sweep-to-bottom">Carrier</a></li>
          </ul>
        </div>
        <div style="clear:both;"></div>
      </header>

      <div class="col-lg-12 col-sm-12" style="background:#fff; padding-top:15px; padding-bottom:15px; margin-bottom:50px;" >
        <div class="col-lg-12 col-sm-12" style="padding:0px; margin-bottom:15px;">
            <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden; ">

              <!-- Loading Screen -->
              <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                  <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                      background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
                  </div>
                  <div style="position: absolute; display: block; background: url(<?=base_url() ?>public/img/loading.gif) no-repeat center center;
                      top: 0px; left: 0px;width: 100%;height:100%;">
                  </div>
              </div>

              <!-- Slides Container -->
              <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 600px; height: 300px; overflow: hidden;">
                    <div>
                      <img u="image" src="<?php echo base_url() ?>public/img/landscape/02.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/03.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/04.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/05.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/06.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/07.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/08.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/09.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/10.jpg" />
                    </div>
                    <div>
                        <img u="image" src="<?php echo base_url() ?>public/img/landscape/11.jpg" />
                    </div>
              </div>
              
              <!--#region Bullet Navigator Skin Begin -->
              <!-- Help: http://www.jssor.com/development/slider-with-bullet-navigator-jquery.html -->
              <style>
                  /* jssor slider bullet navigator skin 05 css */
                  /*
                  .jssorb05 div           (normal)
                  .jssorb05 div:hover     (normal mouseover)
                  .jssorb05 .av           (active)
                  .jssorb05 .av:hover     (active mouseover)
                  .jssorb05 .dn           (mousedown)
                  */
                  .jssorb05 {
                      position: absolute;
                  }
                  .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
                      position: absolute;
                      /* size of bullet elment */
                      width: 16px;
                      height: 16px;
                      background: url(<?php echo base_url() ?>public/img/b05.png) no-repeat;
                      overflow: hidden;
                      cursor: pointer;
                  }
                  .jssorb05 div { background-position: -7px -7px; }
                  .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
                  .jssorb05 .av { background-position: -67px -7px; }
                  .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }
              </style>
              <!-- bullet navigator container -->
              <div u="navigator" class="jssorb05" style="bottom: 16px; right: 6px;">
                  <!-- bullet navigator item prototype -->
                  <div u="prototype"></div>
              </div>
              <!--#endregion Bullet Navigator Skin End -->
              
              <!--#region Arrow Navigator Skin Begin -->
              <!-- Help: http://www.jssor.com/development/slider-with-arrow-navigator-jquery.html -->
              <style>
                  /* jssor slider arrow navigator skin 12 css */
                  /*
                  .jssora12l                  (normal)
                  .jssora12r                  (normal)
                  .jssora12l:hover            (normal mouseover)
                  .jssora12r:hover            (normal mouseover)
                  .jssora12l.jssora12ldn      (mousedown)
                  .jssora12r.jssora12rdn      (mousedown)
                  */
                  .jssora12l, .jssora12r {
                      display: block;
                      position: absolute;
                      /* size of arrow element */
                      width: 30px;
                      height: 46px;
                      cursor: pointer;
                      background: url(<?php echo base_url() ?>public/img/a12.png) no-repeat;
                      overflow: hidden;
                  }
                  .jssora12l { background-position: -16px -37px; }
                  .jssora12r { background-position: -75px -37px; }
                  .jssora12l:hover { background-position: -136px -37px; }
                  .jssora12r:hover { background-position: -195px -37px; }
                  .jssora12l.jssora12ldn { background-position: -256px -37px; }
                  .jssora12r.jssora12rdn { background-position: -315px -37px; }
              </style>
              <!-- Arrow Left -->
              <span u="arrowleft" class="jssora12l" style="top: 123px; left: 0px;">
              </span>
              <!-- Arrow Right -->
              <span u="arrowright" class="jssora12r" style="top: 123px; right: 0px;">
              </span>
              <!--#endregion Arrow Navigator Skin End -->
              <a style="display: none" href="http://www.jssor.com">Bootstrap Slider</a>
          </div>
        </div>

        <div class="col-lg-3 col-sm-12" style="padding:0px 8px 0px 0px; margin-bottom:15px;">
          <div class="incontent" style="background:#c29e70; height:250px; ">
            <h2 style="background:#A25F08;">English Course for Children</h2>
            <p>Lokasi yang aman untuk anak-anak, setiap kelas tersedia tape recorder, jumlah murid dalam 1 kelas 10 orang, buku yang menarik dan disesuaikan dengan usia anak. </p>
            <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12" style="padding:0px 8px 0px 0px; margin-bottom:15px;">
          <div class="incontent" style="background:#88b9e5; height:250px; ">
            <h2 style="background:#5b93c4;">Kursus Matematika</h2>
            <p>Satu kelas 10 orang, diajar oleh guru lulusan FKIP yang berkualitas, materi mengikuti kurikulum nasional, bagi anak kelas 6 diberi latihan UAN.</p>
            <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12" style="padding:0px 4px 0px 0px; margin-bottom:15px;">
          <div class="incontent" style="background:#9bd3a4; height:250px; ">
            <h2 style="background:#6aba77;">Kursus Menggambar</h2> 
            <p>Anak-anak belajar mewarnai dan menggambar di tempat yang nyaman dan dekat dengan lingkungan, diajar oleh pakar menggambar.</p>
            <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>
        </div>
        <div class="col-lg-3 col-sm-12" style="padding:0px 0px 0px 4px; margin-bottom:15px;">
          <div class="incontent" style="background:#f9e69a; height:250px; ">
            <h2 style="background:#f2d253;">Kursus Ca-Lis-Tung</h2>
            <p>Anak-anak diajar membaca, menulis dan berhitung dengan metode yang mudah dimengerti oleh anak, untuk menguji kemampuan anak membaca maka anak diuji. </p>
            <button type="button" class="btn btn-danger mybutton">Info</button>
          </div>
        </div>

        <div style="clear:both"></div>
        <div class="col-lg-5 col-sm-12" style="padding-left:0px; margin-bottom:15px;">
            <div style="max-height: 135px; display: block;" id="shout">
              <ul><li>
                <span class="date"><small>05.09.2015 22:12</small></span>
                <span class="name"><small>cialis:</small></span><br>
                <span class="message"><small>Hello!
                <a href="http://viagrapills99.com/">cheap viagra pills</a> , <a href="http://cialispills99.com/">cheap cialis pills</a> , <a href="http://cialispills91.com/">cialis</a> , <a href="http://viagraprof9.com/">viagra professional</a> , </small></span>
              </li><li>
                <span class="date"><small>03.09.2015 21:40</small></span>
                <span class="name"><small>cialis:</small></span><br>
                <span class="message"><small>
                <a href="http://viagra99withoutprescription.com/">viagra</a> Tablets For Sale Uk in San Juan .   Anniversary <a href="http://cialis99withoutprescription.com/">cialis</a> Vancouver Houses For Sale cash loan.   </small></span>
              </li><li>
                <span class="date"><small>31.08.2015 06:41</small></span>
                <span class="name"><small>discount_cialis:</small></span><br>
                <span class="message"><small>Hello!
               <a href="http://discountgeneric8viagra.com/">discount viagra</a> , <a href="http://discountgeneric8cialis.com/">discount cialis</a> , </small></span>
              </li><li>
                <span class="date"><small>30.08.2015 22:31</small></span>
                <span class="name"><small>without:</small></span><br>
                <span class="message"><small>Hello!
                <a href="http://discountgeneric8viagra.com/">viagra without prescription</a> , <a href="http://discountgeneric8cialis.com/">cialis purchase online</a> , </small></span>
              </li><li>
                <span class="date"><small>30.08.2015 14:16</small></span>
                <span class="name"><small>generic_cialis:</small></span><br>
                <span class="message"><small>Hello!
                <a href="http://discountgeneric8viagra.com/">generic viagra</a> , <a href="http://discountgeneric8cialis.com/">generic cialis</a> , </small></span>
              </li><li>
                <span class="date"><small>28.08.2015 13:20</small></span>
                <span class="name"><small>sqkjjihv:</small></span><br>
                <span class="message"><small>2rand[0,1,1]</small></span>
              </li><li>
                <span class="date"><small>25.08.2015 09:32</small></span>
                <span class="name"><small>nhrzjz:</small></span><br>
                <span class="message"><small>2rand[0,1,1]</small></span>
              </li><li>
                <span class="date"><small>22.08.2015 16:50</small></span>
                <span class="name"><small>cialis_price:</small></span><br>
                <span class="message"><small>
                Safe -  <a href="http://viagra8discountprice.com/">viagra</a> reviews uk only for brand cialis ajanta review name MedStore.   The region best dose for daily <a href="http://cialis8discountprice.com/">cialis price</a> will now promote.   </small></span>
              </li><li>
                <span class="date"><small>21.08.2015 18:44</small></span>
                <span class="name"><small>pvhtpb:</small></span><br>
                <span class="message"><small>2rand[0,1,1]</small></span>
              </li><li>
                <span class="date"><small>20.08.2015 16:16</small></span>
                <span class="name"><small>pojdsf:</small></span><br>
                <span class="message"><small>2rand[0,1,1]</small></span>
              </li></ul><p style="margin-top: 10px; margin-left: 10px;"><a href="http://www.sttiijogjakarta.com/shoutbox">Tampilkan semua</a></p>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" id="usr" placeholder="Nama">
              <input type="text" class="form-control" id="pwd" placeholder="Pesan">
              <button type="button" class="btn btn-warning">Shout!</button>
            </div>
        </div>
        <div class="col-lg-7 col-sm-12" style="padding:0px;">
          <iframe style="width:100%; height:300px;" src="https://www.youtube.com/embed/VQVxMy4qojk" frameborder="0" allowfullscreen></iframe>
        </div>
      </div>
  </div>

  <script type="javascript/text" src="<?=base_url()?>public/js/jquery-latest.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>public/js/jssor.slider.js"></script>
  <script type="javascript/text">

      jQuery(document).ready(function ($) {

          var _SlideshowTransitions = [
          //Fade
          { $Duration: 1200, $Opacity: 2 }
          ];

          var options = {
              $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
              $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
              $AutoPlayInterval: 3000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
              $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

              $ArrowKeyNavigation: true,                    //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
              $SlideDuration: 500,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
              $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
              //$SlideWidth: 600,                                 //[Optional] Width of every slide in pixels, default value is width of 'slides' container
              //$SlideHeight: 300,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
              $SlideSpacing: 0,                           //[Optional] Space between each slide in pixels, default value is 0
              $DisplayPieces: 1,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
              $ParkingPosition: 0,                                //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
              $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
              $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
              $DragOrientation: 3,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

              $SlideshowOptions: {                                //[Optional] Options to specify and enable slideshow or not
                  $Class: $JssorSlideshowRunner$,                 //[Required] Class to create instance of slideshow
                  $Transitions: _SlideshowTransitions,            //[Required] An array of slideshow transitions to play slideshow
                  $TransitionsOrder: 1,                           //[Optional] The way to choose transition to play slide, 1 Sequence, 0 Random
                  $ShowLink: true                                    //[Optional] Whether to bring slide link on top of the slider when slideshow is running, default value is false
              },

              $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                  $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                  $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                  $AutoCenter: 1,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                  $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                  $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                  $SpacingX: 10,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                  $SpacingY: 10,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                  $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
              },

              $ArrowNavigatorOptions: {
                  $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                  $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                  $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
              }
          };
          var jssor_slider1 = new $JssorSlider$("slider1_container", options);

          //responsive code begin
          //you can remove responsive code if you don't want the slider scales while window resizes
          function ScaleSlider() {
              var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
              if (parentWidth)
                  jssor_slider1.$ScaleWidth(Math.min(parentWidth, 600));
              else
                  window.setTimeout(ScaleSlider, 30);
          }
          ScaleSlider();

          $(window).bind("load", ScaleSlider);
          $(window).bind("resize", ScaleSlider);
          $(window).bind("orientationchange", ScaleSlider);
          //responsive code end
      });
  </script>

</body>
</html>
