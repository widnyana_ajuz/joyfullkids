<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_admin");
        $this->load->model("m_home");

        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));

        $this->get_user_login();
    }

    public function index(){
    	$data['slideshow'] = $this->m_admin->get_slideshow();
    	$data['homeitem'] = $this->m_admin->get_homeitem();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	
        $this->load->view('admin/admin', $data);
    }

    protected function get_user_login() {
		// get user login
		$session = $this->session->userdata('session_operator');
		if (!empty($session)) {
			$this->user = $session;
		} else {
			redirect('login');
		}
	}

    public function NewsArticles(){
    	$this->load->library('pagination');

    	$config['base_url'] = base_url().'admin/newsArticles';
        $config['total_rows'] = $this->db->get('articles')->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 20;
        $config['use_page_numbers'] = true;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

    	$query = $this->db->get('articles', $config['per_page'], $this->uri->segment(3));
    	$data['records'] = $query->result_array();
    	$data['links']  = $this->pagination->create_links();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/articles', $data);
    }

    public function contact(){
    	$data['pendaftar'] = $this->m_admin->get_pendaftar();
    	$data['setting'] = $this->m_admin->get_setting();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/contact', $data);
    }

    public function image(){
    	$data['slideshow'] = $this->m_admin->get_image();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/image', $data);
    }

    public function setting(){
    	$data['setting'] = $this->m_admin->get_setting();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/setting', $data);
    }

    public function content(){
    	$data['records'] = $this->m_admin->get_allcontent();
    	$data['nav_menu'] = $this->m_admin->get_nav_menu();

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/content', $data);
    }

    public function program(){
    	$data['records'] = $this->m_admin->get_allprograms();;
    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/program', $data);
    }

    public function gallery(){
    	$album = $this->m_admin->get_album();

    	$real = [];
    	$index = 0;
    	foreach ($album as $value) {
    		$value['cover'] = $this->m_admin->get_newest_photo($value['id']);
    		$value['number'] = $this->m_admin->get_total_photo($value['id']);
    		$real[$index] = $value;
    		$index++;
    	}

    	$data['album'] = $real;

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/gallery', $data);
    }

    public function galeryphoto($id){
    	$data['album_id'] = $id;
    	$data['album'] = $this->m_admin->get_albumphoto($id);

    	$hdata['setting'] = $this->m_home->get_setting();
    	$this->load->view('base/admin-header',$hdata);
    	$this->load->view('admin/galleryphoto', $data);
    }

    public function hapus_slide($id){
    	$delete = $this->m_admin->hapus_slide($id);

		$result = $this->m_admin->get_slideshow();

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function hapus_image($id){
    	$delete = $this->m_admin->hapus_image($id);

		$result = $this->m_admin->get_image();

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function hapus_item($id){
    	$delete = $this->m_admin->hapus_item($id);

		$result = $this->m_admin->get_homeitem();

		header('Content-Type: application/json');
		echo json_encode($result);
    }

    public function upload_slide(){
    	$url = $this->do_upload();
    	$insert['file_path'] = $url;
    	$insert['keterangan'] = $_POST['keterangan'];

    	$this->m_admin->save_slide($insert);

    	redirect('admin');
    }

    public function upload_image(){
    	$url = $this->do_upload();
    	$insert['file_path'] = $url;
    	$insert['alt'] = $_POST['keterangan'];

    	$this->m_admin->save_image($insert);

    	redirect('admin/image');
    }

    public function do_upload(){
		$type = explode('.', $_FILES['gambar']['name']);
		$type = $type[count($type)-1];
		$name = uniqid(rand()).".".$type;
		$url = "public/img/landscape/".$name;
		
		if(in_array($type, array("jpg", "jpeg", "gif", "png")))
			if(is_uploaded_file($_FILES["gambar"]["tmp_name"]))
				if(move_uploaded_file($_FILES["gambar"]["tmp_name"], $url))
					return $name;

		return "";
	}

	public function save_menu(){
		$insert['judul'] = $_POST['judul'];
		$insert['link'] = $_POST['link'];
		$insert['keterangan'] = $_POST['keterangan'];
		$insert['warna_header'] = $_POST['warna_header'];
		$insert['warna_body'] = $_POST['warna_body'];
		$id = $_POST['item_id'];
		
		if($_POST['status']=='insert')
			$this->m_admin->save_item($insert);
		else if($_POST['status']=='update')
			$this->m_admin->update_item($id,$insert);
		
		redirect('admin');
	}

	public function tambah_album(){
		$insert['nama_album'] = $_POST['album'];
		$insert['tahun'] = date('Y');

		$this->m_admin->tambah_album($insert);
		redirect('admin/gallery');
	}

	public function tambah_album_photo(){
		if($_POST['album']=="")
			$insert['file'] = $this->do_upload();
		else
			$insert['file'] = $_POST['album'];

		$insert['album_id'] = $_POST['album_id'];
		$insert['keterangan_foto'] = $_POST['keterangan'];

		$this->m_admin->save_album_foto($insert);

		redirect('admin/galeryphoto/'.$insert['album_id']);
	}

	public function save_articles(){
		$insert['content'] = $_POST['editor1'];
		$insert['judul'] = $_POST['ar_judul'];
		$insert['path'] = $_POST['ar_path'];
		$insert['review'] = $_POST['ar_article'];
		$insert['waktu_pembuatan'] = date('Y-m-d H:i:s');

		$id = $_POST['ar_id'];
		
		if($_POST['status']=='insert'){
			$this->m_admin->save_article($insert);
		}else{
			$this->m_admin->update_article($id, $insert);
		}

		redirect('admin/newsArticles');
	}

	public function save_content(){
		$insert['content'] = $_POST['editor1'];
		$insert['judul'] = $_POST['c_judul'];
		$insert['path'] = $_POST['c_path'];
		$insert['waktu_pembuatan'] = date('Y-m-d H:i:s');

		$id = $_POST['c_id'];

		if($_POST['status']=='insert'){
			$this->m_admin->save_content($insert);
		}else{
			$this->m_admin->update_content($id, $insert);
		}

		redirect('admin/content');
	}

	public function save_navmenu(){
		$insert['id_menu'] = $_POST['m_id'];

		$insert['id_parent'] = 0;
		if($_POST['p_id']!="")
			$insert['id_parent'] = $_POST['p_id'];
		
		$insert['menu'] = $_POST['m_judul'];
		$insert['link'] = $_POST['m_path'];

		$id = $_POST['old_id'];

		if($_POST['m_status']=='insert'){
			$this->m_admin->save_menu($insert);
		}else{
			$this->m_admin->update_menu($id, $insert);
		}

		redirect('admin/content');
	}

	public function get_content($id){
		$result = $this->m_admin->get_content($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_contentmenu($id){
		$result = $this->m_admin->get_contentmenu($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}
	
	public function save_contact(){
		$insert['contact_us'] = $_POST['editor1'];

		$this->m_admin->update_contact($insert);
		redirect('admin/contact');
	}

	public function save_program(){
		foreach ($_POST as $data) {
			$insert  = $data;
		}
		$id = $this->m_admin->get_idprogram()['id'];
		if(empty($id))$id=1;else$id++;

		$insert['id'] = $id;

		$this->m_admin->save_program($insert);
		
		header('Content-Type: application/json');
		echo json_encode($id);
	}

	public function save_program_syarat(){
		foreach ($_POST as $data) {
			$insert  = $data;
		}

		$this->m_admin->save_program_syarat($insert);
	}

	public function save_program_keunggulan(){
		foreach ($_POST as $data) {
			$insert  = $data;
		}

		$this->m_admin->save_program_keunggulan($insert);
	}

	public function save_program_jadwal(){
		foreach ($_POST as $data) {
			$insert  = $data;
		}

		$this->m_admin->save_program_jadwal($insert);
	}

	public function hapus_daftar($id){
		$delete = $this->m_admin->hapus_daftar($id);

		$result = $this->m_admin->get_pendaftar();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_content($id){
		$delete = $this->m_admin->hapus_content($id);

		$result = $this->m_admin->get_allcontent();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_menu($id){
		$delete = $this->m_admin->hapus_menu($id);

		$result = $this->m_admin->get_nav_menu();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function hapus_program($id){
		$delete = $this->m_admin->hapus_program($id);

		$result = $this->m_admin->get_allprograms();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_keunggulan($id){
		$result = $this->m_admin->get_keunggulan($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_syarat($id){
		$result = $this->m_admin->get_syarat($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function get_jadwal($id){
		$result = $this->m_admin->get_jadwal($id);

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function save_setting(){
		if($_POST['youtube']!="")$insert['youtube_link'] = $_POST['youtube'];
		
		if($_POST['background']!="")$insert['background'] = $_POST['background'];

		if(isset($insert))
			$this->m_admin->update_contact($insert);

		redirect('admin/setting');
	}

	public function hapus_photo_album(){
		$album = $_POST['id_album'];
		$photo = $_POST['id_photo'];
		$delete = $this->m_admin->hapus_photo_album($photo);

		$result = $this->m_admin->get_albumphoto($album);

		header('Content-Type: application/json');
		echo json_encode($result);		
	}

	public function hapus_album(){
		$album = $_POST['id_album'];
		
		$delete = $this->m_admin->hapus_photo_on_album($album);
		$delete = $this->m_admin->hapus_album($album);
	}

	public function hapus_article($id){
		$delete = $this->m_admin->hapus_article($id);

		return 'ok';
	}
}

?>