<?php
class Info extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_home");
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
    	redirect(base_url());
    }

    public function detail($info){
    	$hdata['menu'] = $this->m_home->get_menu();
        $data['program'] = $this->m_home->get_program($info);

        if(empty($data['program']))redirect('home');
        
        $id = $data['program']['id'];

        $data['keunggulan'] = $this->m_home->get_keunggulan($id);
        $data['syarat'] = $this->m_home->get_syarat($id);
        $data['jadwal'] = $this->m_home->get_jadwal($id);
        $hdata['setting'] = $this->m_home->get_setting();
        $hdata['submenu'] = $this->m_home->get_submenu();
        $data['homeitem'] = $this->m_home->get_homeitem();
        
    	$this->load->view('base/header', $hdata);
        $this->load->view('info/info',$data);
        $this->load->view('base/footer');
    }
}

?>