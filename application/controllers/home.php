<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_home");
    }

    public function index(){

    	$data['slideshow'] = $this->m_home->get_slideshow();
    	$data['homeitem'] = $this->m_home->get_homeitem();
        $data['setting'] = $this->m_home->get_setting();
        $data['shoutbox'] = $this->m_home->get_shoubox();

        $hdata['menu'] = $this->m_home->get_menu();
        $hdata['submenu'] = $this->m_home->get_submenu();
        $hdata['setting'] = $this->m_home->get_setting();
        
        // print_r($hdata['submenu']);die;
    	$this->load->view('base/header', $hdata);
        $this->load->view('home/home', $data);
        $this->load->view('base/footer');
    }

    public function submit_shout(){
        foreach ($_POST as $data) {
            $insert = $data;
        }

        $insert['waktu'] = date('Y-m-d h:i:s');

        $this->m_home->save_shout($insert);
        $result = $this->m_home->get_shoubox();

        header('Content-Type: application/json');
        echo json_encode($result);
    }
}

?>