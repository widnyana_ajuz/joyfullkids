<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_contact");
        $this->load->model("m_home");
    }

    public function index(){
        $data['setting'] = $this->m_contact->get_setting();

    	$hdata['menu'] = $this->m_home->get_menu();
        $hdata['setting'] = $this->m_home->get_setting();
        $hdata['submenu'] = $this->m_home->get_submenu();
        $data['homeitem'] = $this->m_home->get_homeitem();
        
        $this->load->view('base/header', $hdata);
        $this->load->view('contact/contact', $data);
        $this->load->view('base/footer');
    }

    public function send_contact(){
        $insert['nama_daftar'] = $_POST['nama_daftar'];
        $insert['nama_ortu'] = $_POST['nama_ortu'];
        $insert['alamat'] = $_POST['alamat'];
        $insert['no_telp'] = $_POST['no_telp'];
        $insert['email'] = $_POST['email'];
        $insert['nama_anak'] = $_POST['nama_anak'];
        $insert['tgl_lahir'] = $_POST['tgl_lahir'];
        $insert['umur'] = $_POST['umur'];
        $insert['kelas_tujuan'] = $_POST['kelas_tujuan'];
        $insert['pesan'] = $_POST['pesan'];
        
        $this->m_contact->send_contact($insert);
        redirect('contact');
    }
}

?>