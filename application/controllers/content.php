<?php
class Content extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_home");
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
    	redirect(base_url());
    }

    public function info($path){
    	$hdata['menu'] = $this->m_home->get_menu();
        $data['content'] = $this->m_home->get_content($path);
        $data['homeitem'] = $this->m_home->get_homeitem();
        
        $hdata['setting'] = $this->m_home->get_setting();
        $hdata['submenu'] = $this->m_home->get_submenu();
        
    	$this->load->view('base/header', $hdata);
        $this->load->view('news/detail', $data);
        $this->load->view('base/footer');
    }
}

?>