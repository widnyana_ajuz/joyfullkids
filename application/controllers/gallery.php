<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_contact");
        $this->load->model("m_home");
        $this->load->model("m_admin");
    }

    public function index(){
        $data['setting'] = $this->m_contact->get_setting();

    	$hdata['menu'] = $this->m_home->get_menu();
        $hdata['setting'] = $this->m_home->get_setting();
        $hdata['submenu'] = $this->m_home->get_submenu();
        $data['homeitem'] = $this->m_home->get_homeitem();

        $album = $this->m_admin->get_album();

        $real = [];
        $index = 0;
        foreach ($album as $value) {
            $value['cover'] = $this->m_admin->get_newest_photo($value['id']);
            $value['number'] = $this->m_admin->get_total_photo($value['id']);
            $real[$index] = $value;
            $index++;
        }

        $data['album'] = $real;
        
        $this->load->view('base/header', $hdata);
        $this->load->view('content/gallery', $data);
        $this->load->view('base/footer');
    }

    public function photo($id){
        $data['setting'] = $this->m_contact->get_setting();

        $hdata['menu'] = $this->m_home->get_menu();
        $hdata['setting'] = $this->m_home->get_setting();
        $data['homeitem'] = $this->m_home->get_homeitem();

        $data['album_id'] = $id;
        $data['album'] = $this->m_admin->get_albumphoto($id);
        $data['nama_album'] = $this->m_admin->get_namaalbum($id)['nama_album'];

        $this->load->view('base/header', $hdata);
        $this->load->view('content/galleryphoto', $data);
        $this->load->view('base/footer');
    }

}