<?php
class News extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_news");
        $this->load->model("m_home");
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
    	$this->load->library('pagination');

    	redirect('news/articles');
    }

    public function articles(){
    	$this->load->library('pagination');

    	$config['base_url'] = base_url().'news/articles';
        $config['total_rows'] = $this->db->get('articles')->num_rows();
        $config['per_page'] = 10;
        $config['num_links'] = 30;
        $config['use_page_numbers'] = true;

        $this->load->library('pagination');
        $this->pagination->initialize($config);

    	$query = $this->db->get('articles', $config['per_page'], $this->uri->segment(3));
    	$data['records'] = $query->result_array();
    	$data['link']  = $this->pagination->create_links();
        $hdata['setting'] = $this->m_home->get_setting();
    	$hdata['menu'] = $this->m_home->get_menu();
        $hdata['submenu'] = $this->m_home->get_submenu();
        $data['homeitem'] = $this->m_home->get_homeitem();

        $this->load->view('base/header', $hdata);
    	$this->load->view('news/news', $data);
        $this->load->view('base/footer');
    }

    public function detail($path){
    	$data['content'] = $this->m_news->get_content($path);
        $hdata['menu'] = $this->m_home->get_menu();
        $data['homeitem'] = $this->m_home->get_homeitem();
        $hdata['setting'] = $this->m_home->get_setting();
        
        $this->load->view('base/header', $hdata);
    	$this->load->view('news/detail', $data);
        $this->load->view('base/footer');
    }
}

?>