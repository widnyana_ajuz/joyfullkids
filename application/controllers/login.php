<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller{
	function __construct(){
        parent:: __construct();
        $this->load->model("m_login");
        $this->load->model("m_home");
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index(){
    	if (isset($this->user) && !empty($this->user)) {
			redirect('admin');
		}

		$this->get_user_login();
		$data['message'] = $this->session->flashdata('message');
		$data['setting'] = $this->m_home->get_setting();
    	
    	$this->load->view('login/login', $data);
    }

    protected function get_user_login() {
		// get user login
		$session = $this->session->userdata('session_operator');
		if (empty($session)) {
			$this->user = $session;
		} else {
			redirect('admin');
		}
	}

	public function login_validation()
	{
		//form validation
		$this->form_validation->set_rules('Username', 'username', 'required|max_length[100]');
		$this->form_validation->set_rules('Password', 'password', 'required|sha1|max_length[50]');


		if ($this->form_validation->run() == TRUE) {
			// if validation run
			if ($query = $this->m_login->login_validation(array($this->input->post('Username'), $this->input->post('Password')))) {
				//session register
				$this->session->set_userdata('session_operator', $query);
				
				redirect('admin');

				redirect($query['role_default_url']);

			} else {
				$this->session->set_flashdata('message', 'Akun anda tidak dapat ditemukan');
			}
		} else {
			$this->session->set_flashdata('message', validation_errors());
		}

		redirect('login');
	}

	// logout
	public function logout() {
		// log history
		//$this->m_login->user_log_leave(array($this->session->userdata('session_operator')['user_id']));
		// destroy the session
		$this->session->unset_userdata('session_operator');

		redirect('login');
	}


}