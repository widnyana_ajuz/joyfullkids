$(document).ready(function(){
	$("#logo-front").hide();
	$(".input-type").hide();
	$("#headerslide").hide();
	$(".submit").hide();

	$("#container-log").animate({width:'100%', opacity: '0.5'}, "slow");
	$("#container-log").animate({height:'100%', opacity: '1'}, "slow")
	.promise()
	.done(function(){
		$("#logo-front").slideDown()
		.promise()
		.done(function(){
			$(".input-type").fadeIn();
			$("#headerslide").fadeIn();
			$(".submit").fadeIn();
		});
	});
});

$(".submit").click(function(){
	var username = document.getElementById("username").value;
	var password = document.getElementById("password").value;
	if(username==password){
		$("#logo-front").hide();
		$(".input-type").hide();
		$("#headerslide").hide();
		$(".submit").hide();
		$("#container-log").animate({height:'10px', opacity: '1'}, "slow")
		$("#container-log").animate({width:'0', opacity: '0.5'}, "slow")
		.promise()
		.done(function(){
			location.href="home.html";
		});
	}
	else{
		alert("Gagal login, Username dan Password harus sama");
	}
});